###############################################################################
# (c) Copyright 2012-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from ProdConf import ProdConf

ProdConf(
    Application="DaVinci",
    AppVersion="v30r0",
    InputFiles=["LFN:/lhcb/.../00012503_00000503_1.sdst"],
    OutputFilePrefix="00016310_00000012_1",
    OutputFileTypes=["stream1.sdst", "stream2.sdst"],
    XMLFileCatalog="pool_xml_catalog.xml",
    XMLSummaryFile="summaryDaVinci_00016310_00000012_1.xml",
    HistogramFile="DaVinci_00016310_00000012_1_Hist.root",
    DDDBTag="head-20110914",
    CondDBTag="head-20111102")
