The tests of ProdConf
=====================

After any change in the ProdConf package, the code has to be validated using the
embedded tests.  The test suite should also be extended to cover the new use
cases that are handled.

The tests directory contain two groups of tests _Unit_ and _Integration_.

Unit tests are meant to validate the code of ProdConf using mock classes to
mimic the behavior of other configurables classes.
The tests should be written and extended such that we have a complete coverage
of the ProdConf code.  Of course, this does not guarantee that the code is
correct, but spots problems like syntax errors or typos.

Integration tests verify that the behavior of ProdConf is the expected one when
used on "real-life" tests.  In this case, the real application are configured
and, often, run, to ensure that the execution produces what is expected.

During the development phase, the Unit test should be run regularly, since they
are very quick, while the Integration tests take a lot of time, so it's better
to run them once we are confident that the changes are doing what we expect.

Running the tests
-----------------

To run the Unit tests, we need to add to the Python path a couple of entries:
Gaudi and the local version of ProdConf.

Gaudi can be added in two ways:

    # SetupProject --no-user --tag_add=no-pyzip Gaudi

or

    # export PYTHONPATH=/afs/cern.ch/sw/Gaudi/releases/GAUDI/GAUDI_v23r2/InstallArea/x86_64-slc5-gcc43-opt/python:$PYTHONPATH

The local copy of ProdConf should be added explicitly:

    # export PYTHONPATH=/path/to/my/ProdConf/python:$PYTHONPATH


Once the Python path is ready, enter the tests directory and run:

    # nosetests -v Unit


Integration tests only need the path the the local ProdConf, but the path to
Gaudi does not harm, so you can re-use the same environment as for Unit tests.
To run them:

    # nosetests -v Integration


If you do not specify any argument to _nosetests_, both Integration and Unit
tests are run.
