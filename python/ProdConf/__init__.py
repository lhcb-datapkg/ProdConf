###############################################################################
# (c) Copyright 2012-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Module to abstract the production configuration from the detailed configurations
of the specific software projects.
"""
# pylint: disable=W0404
__author__ = "Marco Clemencic <marco.clemencic@cern.ch>"

# Subversion keywords
from ProdConf._helpers import parseSubversionKeywords
__version__ = parseSubversionKeywords("$Revision$", "$URL$")

from ProdConf import WG

# Expose only the configure function
__all__ = ("configure", "ProdConf")

from GaudiKernel.Configurable import Configurable, ConfigurableUser
from GaudiKernel.Configurable import appendPostConfigAction, removePostConfigAction

import logging


class ProdConf(ConfigurableUser):
    """ Configurable class for the configuration of the production jobs.
    """
    # yapf: disable
    # Properties of the configurable.
    __slots__ = {"Application": "",
                 "AppVersion": "",
                 "OptionFormat": "",
                 "NThreads": 1,

                 "InputFiles": [],
                 "OutputFilePrefix": "",
                 "OutputFileTypes": [],

                 "XMLFileCatalog": "pool_xml_catalog.xml",
                 "XMLSummaryFile": "summary%(Application)s_%(OutputFilePrefix)s.xml",
                 "HistogramFile": "", # "%(Application)s_%(OutputFilePrefix)s_Hist.root",

                 "DDDBTag": "",
                 "CondDBTag": "",
                 "DQTag": "",

                 "NOfEvents": -1,

                 "TCK" : "",

                 "RunNumber": 0,
                 "FirstEventNumber": 0,

                 "ProcessingPass": "",

                 "_enabledConfUsers": []
                 }

    # Doc-strings for the properties.
    _propertyDocDct = {"Application": "name of the software application (Brunel, Gauss, DaVinci, etc.)",
                       "AppVersion": "(optional) version number of the application",
                       "OptionFormat": "(optional) switch on special hacks required in the option files",
                       "NThreads": "(optional) set number of threads available",

                       "InputFiles": "(optional) list of input file names (LFNs or PFNs)",
                       "OutputFilePrefix": "name of the prefix of the output file, defined by the production",
                       "OutputFileTypes": "extensions (without '.') of the output files to be produced, as specified by the step",

                       "XMLFileCatalog": "(optional) name of the XML file catalog, a template can be used",
                       "XMLSummaryFile": "(optional) name of the XML file summary, a template can be used",
                       "HistogramFile": "(optional) name of the histogram file, a template can be used",

                       "DDDBTag": "tag for the Detector Description Database",
                       "CondDBTag": "tag for the Conditions Database",
                       "DQTag": "tag for the Data Quality Flags Database",


                       "NOfEvents": "number of events to be processed (-1 for all events in the input)",

                       "TCK": "(optional) TCK used as input to Moore jobs - specifically the prompt swimming",
                       "SwimmingDebug" : "(optional) Toggle running Swimming in debug mode, default is False",

                       "RunNumber": "run number used in the job",
                       "FirstEventNumber": "first event number used in the simulation",

                       "ProcessingPass": "name of the output processing pass",

                       "_enabledConfUsers": "internal record of the ConfigurableUser instances active at ProdConf construction"}

    # Known application names.
    __applications__ = ("Gauss",
                        "Boole",
                        "Moore",
                        "Brunel",
                        "DaVinci",
                        "Noether",
                        "LHCb",
                        "Castelao")
    # yapf: enable

    __used_configurables__ = [WG.WGProd]

    def __init__(self, name=Configurable.DefaultName, _enabled=True, **kwargs):
        # the 'Application' property needs to be treated in a special way because
        # of the special action in the callback function
        if "Application" in kwargs:
            app = kwargs["Application"]
            del kwargs["Application"]
        else:
            app = None
        # Workaround for GAUDI-1304
        from Gaudi.Configuration import allConfigurables
        kwargs["_enabledConfUsers"] = [
            c for c in allConfigurables.values()
            if hasattr(c, '_enabled') and c._enabled
        ]
        # base class constructor
        super(ProdConf, self).__init__(name, _enabled, **kwargs)
        # now we can actually set the application property
        if app:
            self.Application = app

    def _applicationSet(self, value):
        """
        Method called to validate the value of the property 'Application'.
        """
        if value not in self.__applications__:
            raise ValueError(
                "Invalid value for 'Application' property: %r" % value)
        if hasattr(self, "Application"):
            # Application name already set
            if self.Application != value:
                logging.getLogger("ProdConf").warning(
                    "Changing application for production from %r to %r",
                    self.Application, value)
            else:
                # no need to do anything if we try to set the app name to the value
                # it already has
                return
        # bind the ConfigurableUser instance to the Application configurable
        import Configurables
        confname = value
        if confname in ('LHCb', 'Noether'):
            confname = 'LHCbApp'
        elif confname == 'Castelao':
            confname = 'DaVinci'
        appConf = getattr(Configurables, confname)
        if confname in Configurable.allConfigurables:
            self._addActiveUseOf(Configurable.allConfigurables[confname])
        else:
            self._addActiveUseOf(appConf(_enabled=False))

        # Some configurations require that some special ConfigurableUser classes
        # are instantiated at this level (cannot be instantiated during
        # __apply_configuration__ because of a limitation in the design of that
        # code in Gaudi).
        # Moreover, we need to declare the dependency on them to avoid that they
        # get triggered before ProdConf.
        class DSTWriterConstructor(object):
            """
            Helper class to get the DST Writers ConfigurableUsers from
            DSTWriters.__dev__ (DaVinci v29r2) or from DSTWriters (DaVinci v29r3).
            """

            def __init__(self, clsName):
                self.clsName = clsName

            def __call__(self, *args, **kwargs):
                """
                Instantiate the required DSTWriter from DSTWriters.__dev__ if
                that module exists, otherwise fall back on DSTWriters.
                """
                try:
                    import DSTWriters.__dev__.Configuration as Writers
                except ImportError:
                    import DSTWriters.Configuration as Writers
                return getattr(Writers, self.clsName)(*args, **kwargs)

        # Special required ConfigurableUser instances:
        # {App: {name: constructor_or_typename, ...}, ...}
        confUsers = {}
        confUsers["LHCbApp"] = {"RawEventJuggler": "RawEventJuggler"}
        confUsers["Moore"] = {"Swimming": "Swimming", "L0App": "L0App"}
        confUsers["DaVinci"] = {
            "Swimming": "Swimming",
            "KaliPi0Conf": "KaliPi0Conf",
            "Tesla": "Tesla",
            "MyDSTWriter": DSTWriterConstructor("SelDSTWriter"),
            "FullDST": DSTWriterConstructor("SelDSTWriter"),
            "MicroDST": DSTWriterConstructor("MicroDSTWriter"),
            "WGProd": "WGProd"
        }
        confUsers["Castelao"] = {
            "WGProd": "WGProd",
            "MyDSTWriter": DSTWriterConstructor("SelDSTWriter"),
        }
        if confname in confUsers:
            for n in confUsers[confname]:
                if n in Configurable.allConfigurables:
                    # it has already been instantiated
                    self._addActiveUseOf(Configurable.allConfigurables[n])
                else:
                    # not yet instantiated... it should not happen, but it's safer
                    # to declare the dependency anyway
                    c = confUsers[confname][n]
                    if type(c) is str:
                        # sometimes the required class is not present
                        # (e.g. for old version of DaVinci)
                        if hasattr(Configurables, c):
                            c = getattr(Configurables, c)
                        else:
                            continue
                    self._addActiveUseOf(c(n, _enabled=False))

        # FIXME: hack can be removed starting from Analysis > v10r0
        try:
            # Hack to address the issue described in task #30221
            # https://savannah.cern.ch/task/?30221
            import KaliCalo.Configuration
            removePostConfigAction(KaliCalo.Configuration.action)
            # we store some flag in the module to keep track of the behavior
            KaliCalo.Configuration._prodconf_install_action = True
        except (AttributeError, ValueError):
            # if KaliCalo.Configuration is fixed, we do not need to install the action
            KaliCalo.Configuration._prodconf_install_action = False
        except ImportError:
            # KaliCalo is not in the path, no need to do anything
            pass

    # map of callback functions used for the properties
    __callbacks__ = {"Application": _applicationSet}

    def __setattr__(self, name, value):
        """
        This is the only way to trap calls to the property setters because the
        implementation of PropertyProxy and Configurable prevents re-wrapping of
        the properties.
        FIXME: MCl
        """
        cb = self.__callbacks__.get(name)
        if cb:
            cb(self, value)
        super(ProdConf, self).__setattr__(name, value)

    def _addActiveUseOf(self, other):
        """
        Wrapping around ConfigurableUser.__addActiveUseOf, needed (and implemented
        this way) because of the way Python decorates methods starting with "__"
        and the implementation of Configurable.__getattr__.
        FIXME: MCl
        """
        # pylint: disable=E1101
        ConfigurableUser._ConfigurableUser__addActiveUseOf(self, other)

    def _outputFileName(self):
        """
        Helper function for the cases where a single output file is needed.

        Return None if the output file is not defined.
        """
        # sanity check
        for p in ["OutputFilePrefix", "OutputFileTypes"]:
            if not self.isPropertySet(p):
                return None
        if len(self.OutputFileTypes) != 1:
            raise ValueError(
                "Invalid number of OutputFileTypes for %s (must be 1)" %
                self.Application)
        return self.OutputFilePrefix + '.' + self.OutputFileTypes[0].lower()

    def _appVersion(self):
        """
        Return the version of the the application as a tuple, to simplify the
        comparison.
        """

        def toInt(n):
            "Helper function to convert to int, translating None in 0"
            if n is None: return 0
            else: return int(n)

        import re
        m = re.match(r"v(\d*)r(\d*)(?:p(\d*))?(?:g(\d*))?",
                     self.getProp("AppVersion"))
        if m is not None:
            return tuple([toInt(i) for i in m.groups()])
        else:
            return None

    def _Gauss(self):
        """
        Gauss-specific configuration.
        """
        majorVersion = self._appVersion()[0]
        if majorVersion >= 60:
            from Configurables import Gauss
            Gauss().RunNumber = self.getProp("RunNumber")
            Gauss().FirstEventNumber = self.getProp("FirstEventNumber")
            Gauss().EnableHive = True
            Gauss().ThreadPoolSize = self.getProp("NThreads")
            Gauss().EventSlots = self.getProp("NThreads")
            Gauss().EvtMax = self.getProp("NOfEvents")
        else:
            from Configurables import GenInit, Gauss
            Gauss()
            GenInit(
                "GaussGen",
                RunNumber=self.getProp("RunNumber"),
                FirstEventNumber=self.getProp("FirstEventNumber"))

        file_types = [t.lower() for t in self.OutputFileTypes]
        if not file_types:
            raise ValueError("Min 1 OutputFileTypes required")
        if len(file_types) > 2:
            raise ValueError("Max 2 OutputFileTypes supported")
        standard_types, tuple_types = [], []
        for file_type in file_types:
            (standard_types,
             tuple_types)[file_type.endswith('mctuple.root')].append(file_type)
        # standard Gauss output
        if standard_types:
            if len(standard_types) > 1:
                raise ValueError("Max 1 standard Gauss output accepted")
            from Configurables import OutputStream
            OutputStream("GaussTape").Output = ".".join((self.OutputFilePrefix,
                                                         standard_types[0]))
            Gauss().OutputType = standard_types[0]

        # MC NTuples
        if tuple_types:
            if len(tuple_types) > 1:
                raise ValueError("Max 1 ntuple output supported")
            if not standard_types:
                Gauss().OutputType = 'NONE'
            from Configurables import NTupleSvc
            NTupleSvc().Output = [
                "FILE1 DATAFILE='{}.{}' TYP='ROOT' OPT='NEW'".format(
                    self.OutputFilePrefix, tuple_types[0])
            ]

    def _Boole(self):
        """
        Boole-specific configuration.
        """
        from Configurables import Boole, OutputStream
        app = Boole()
        types = [str.upper(t) for t in self.OutputFileTypes]
        if 'DIGI' in types:
            of1 = self.OutputFilePrefix + '.digi'
            OutputStream(
                "DigiWriter"
            ).Output = "DATAFILE='PFN:%s' SVC='RootCnvSvc' OPT='RECREATE'" % of1
        if 'XDIGI' in types:
            app.DigiType = 'Extended'
            types = [t if t != 'XDIGI' else 'DIGI' for t in types]
            of1 = self.OutputFilePrefix + '.xdigi'
            OutputStream(
                "DigiWriter"
            ).Output = "DATAFILE='PFN:%s' SVC='RootCnvSvc' OPT='RECREATE'" % of1
        if 'MDF' in types:
            of2 = self.OutputFilePrefix + '.mdf'
            OutputStream(
                "RawWriter"
            ).Output = "DATAFILE='PFN:%s' SVC='LHCb::RawDataCnvSvc' OPT='RECREATE'" % of2
        app.Outputs = types
        app.DatasetName = self.OutputFilePrefix

    def _Moore(self):
        """
        Moore-specific configuration.
        """
        fmt = self.getProp("OptionFormat").lower()
        if fmt == 'l0app':
            from Configurables import L0App as App
        else:
            from Configurables import Moore as App
        app = App()
        # @fixme: this should not be needed
        if fmt not in ['swimming2011', 'swimming2012', 'l0app']:
            if self.isPropertySet("DDDBTag"):
                app.DDDBtag = self.DDDBTag
            if self.isPropertySet("CondDBTag"):
                app.CondDBtag = self.CondDBTag
        # moore.DQFLAGStag = self.getProp("DQTag")
        if len(self.OutputFileTypes) == 1:
            of = self._outputFileName()
        elif len(self.OutputFileTypes) > 1:
            split = [tuple(x.rsplit(".", 1)) for x in self.OutputFileTypes]
            lens = set(len(s) for s in split)
            if len(lens) > 1:
                raise ValueError("Inconsistent values in OutputFileTypes")
            if lens.pop() != 2:  # no '.' in the OutputFileTypes
                raise ValueError("Different OutputFileTypes not supported")
            _, ext = list(zip(*split))
            if len(set(ext)) != 1:
                raise ValueError("Inconsistent extensions in OutputFileTypes")
            of = self.OutputFilePrefix + ".{stream}." + ext[0].lower()
        if of is not None:
            app.outputFile = of
            if fmt in ["swimming2011", "swimming2012"]:
                from Configurables import LHCbApp, Swimming
                sw = Swimming(
                    OutputFile=of,
                    XMLSummary=LHCbApp().getProp("XMLSummary"),
                    EvtMax=self.getProp("NOfEvents"))
                if fmt == "swimming2011":
                    sw.RunNumber = self.getProp("RunNumber")
                elif fmt == "swimming2012":
                    sw.CondDBtag = self.getProp("CondDBTag")
                    sw.DDDBtag = self.getProp("DDDBTag")
                    sw.TCK = self.getProp("TCK")
                from Swimming.Configuration import ConfigureMoore
                ConfigureMoore()

    def _Brunel(self):
        """
        Brunel-specific configuration.
        """

        from Configurables import Brunel, OutputStream
        from Configurables import LHCbApp

        fmt = self.getProp("OptionFormat").lower()
        of = self._outputFileName()

        if of is not None:
            if fmt == 'vanilla':
                app = LHCbApp()
                from Configurables import InputCopyStream
                ics = InputCopyStream()
                from GaudiConf import IOHelper
                IOHelper().outStream(filename=of, writer=ics, writeFSR=True)
                return
            else:
                app = Brunel()
                OutputStream(
                    "DstWriter"
                ).Output = "DATAFILE='PFN:%s' TYP='POOL_ROOTTREE' OPT='RECREATE'" % of
                # No need to check the content of OutputFileTypes because it's implicit in the previous call
                outputType = self.OutputFileTypes[0].upper()
                if outputType == "FULL.DST":
                    outputType = "DST"
                app.OutputType = outputType
                app.ProductionMode = True

    def _DaVinci(self):
        """
        DaVinci-specific configuration.
        """
        fmt = self.getProp("OptionFormat").lower()
        if fmt == 'wgprod':
            return self._wgprod()

        if fmt not in ['stripping', 'tesla']:
            # Stripping produces several output files
            # and so does the nominal Turbo
            of = self._outputFileName()

        # Special for Turcal and MC
        if fmt in ('tesla'):
            of = ''
            if len(self.OutputFileTypes) == 1:
                of = self._outputFileName()

        # If OptionFormat is Tesla, only configure Tesla
        if fmt in ["tesla"]:
            from Configurables import Tesla as _App
        else:
            from Configurables import DaVinci as _App

        dv = _App()

        from Configurables import LHCbApp, HistogramPersistencySvc
        app = LHCbApp()
        hs = HistogramPersistencySvc()
        if hasattr(app, "EvtMax"):
            dv.EvtMax = app.EvtMax
        if hasattr(hs, "OutputFile"):
            # Different slots in DaVinci and Tesla
            if fmt in ["tesla"]:
                dv.Histogram = hs.OutputFile
            else:
                dv.HistogramFile = hs.OutputFile

        if fmt in ["dq", "merge"]:
            if of is not None:
                from Configurables import InputCopyStream
                ics = InputCopyStream()
                ics.Output = of
                if fmt == "dq":
                    dv.MoniSequence.append(ics)
                else:
                    from Configurables import RecordStream
                    fr = RecordStream("FileRecords")
                    fr.Output = ics.Output

        elif fmt in ["tesla"]:
            # Set stream file names to correct values
            if hasattr(dv, "outputSuffix"):
                dv.outputPrefix = self.OutputFilePrefix + '.'
            if of is not None:
                dv.outputFile = of

        elif fmt in ["swimming2011", 'swimming2012']:
            from Configurables import Swimming
            sw = Swimming(
                XMLSummary=LHCbApp().getProp("XMLSummary"),
                EvtMax=self.getProp("NOfEvents"),
                CondDBtag=self.getProp("CondDBTag"),
                DDDBtag=self.getProp("DDDBTag"))
            if of is not None:
                sw.OutputFile = of
            from Swimming.Configuration import ConfigureDaVinci
            ConfigureDaVinci()

        elif fmt == "femto":
            from KaliCalo.Configuration import KaliPi0Conf
            from Gaudi.Configuration import ERROR
            kp0c = KaliPi0Conf(
                PrintFreq=10000,
                OutputLevel=ERROR,
                EvtMax=self.getProp("NOfEvents"))
            if of is not None:
                kp0c.FemtoDST = of

            # FIXME: hack can be removed starting from Analysis > v10r0
            try:
                # Hack to address the issue described in task #30221
                # https://savannah.cern.ch/task/?30221
                import KaliCalo.Configuration
                if KaliCalo.Configuration._prodconf_install_action:  #@UndefinedVariable
                    appendPostConfigAction(KaliCalo.Configuration.action)
            except (AttributeError, ImportError):
                # if KaliCalo.Configuration is fixed or not available, we get an error that can be ignored
                pass

        elif (len(self.OutputFileTypes) and
              (self.OutputFileTypes[0].lower().endswith('hltfilter.mdst')
               or self.OutputFileTypes[0].lower().endswith('hltfilter.dst'))):
            # Special case for Turbo filtering
            from GaudiConf.IOHelper import IOHelper
            of = self.OutputFilePrefix + "." + self.OutputFileTypes[0].lower()
            IOHelper().outputAlgs(
                filename=of, writer='InputCopyStream/INPUTCOPY', writeFSR=True)

        else:
            from Configurables import SelDSTWriter, InputCopyStream
            from GaudiConf import IOHelper
            if self.isPropertySet("OutputFilePrefix"):
                SelDSTWriter(
                    "MyDSTWriter").OutputFileSuffix = self.OutputFilePrefix
                # hack to manage MDSTDST output in stripping20r{0,1}p3
                if 'mdst.dst' in self.getProp("OutputFileTypes"):
                    icStream = InputCopyStream("MDSTDSTWriter")
                    of = self.OutputFilePrefix + ".Mdst.dst"
                    IOHelper().outputAlgs(
                        filename=of, writer=icStream, writeFSR=True)

    def _LHCb(self):
        """
        LHCb-specific configuration.
        """
        # Note: LHCb by itself is used only to merge MC data and for the indexer
        from Configurables import ApplicationMgr, LbAppInit, LHCbApp
        LHCbApp()
        ApplicationMgr().TopAlg.insert(0, LbAppInit())

        fmt = self.getProp("OptionFormat").lower()
        of = self._outputFileName()

        if fmt == "indexer":
            from Configurables import EventIndexer
            eidx = EventIndexer()
            eidx.Stripping = self.getProp('ProcessingPass')
            if of:
                eidx.OutputFile = of
        else:  # default: merging
            if of is not None:
                try:
                    from Configurables import InputCopyStream
                    ics = InputCopyStream()
                except ImportError:
                    from Configurables import OutputStream
                    ics = OutputStream("InputCopyStream")
                ics.Output = "DATAFILE='PFN:%s' TYP='POOL_ROOTTREE' OPT='RECREATE'" % of

    def _Noether(self):
        '''
        Noether-specific configuration.
        '''
        fmt = self.getProp("OptionFormat").lower()

        if fmt == "raweventjuggler":
            from Configurables import ApplicationMgr, LbAppInit, LHCbApp
            LHCbApp()
            ApplicationMgr().TopAlg.insert(0, LbAppInit())

            of = self._outputFileName()
            if of is None:
                raise ValueError(
                    "LHCb/RawEventJuggler setup error: no output filename")

            try:
                from Configurables import InputCopyStream
                ics = InputCopyStream()
            except ImportError:
                from Configurables import OutputStream
                ics = OutputStream("InputCopyStream")

            from Configurables import RawEventJuggler
            RawEventJuggler().WriterOptItemList = ics
            from GaudiConf import IOHelper
            IOHelper().outStream(of, writer=ics, writeFSR=True)
        else:
            # At the moment Noether is a thin wrapper around LHCb
            self._LHCb()

    def _Castelao(self):
        fmt = self.getProp("OptionFormat").lower()
        if fmt == 'wgprod':
            self._wgprod()

    def _wgprod(self):
        self.propagateProperties(others=WG.WGProd())

    def abortStalledEvents(self, timeout=3600):
        '''
        Enable the StalledEventMonitor service turning on the abort of stalled
        events.

        @param timeout: time budget for an event before abort (in seconds)
        '''
        try:
            from Configurables import ApplicationMgr
            app = ApplicationMgr()
            app.StalledEventMonitoring = True  # enable the StalledEventMonitor service

            from Configurables import StalledEventMonitor
            em = StalledEventMonitor()
            if not em.isPropertySet('EventTimeout'):
                em.EventTimeout = timeout  # wait timeout seconds before reporting
            em.StackTrace = True  # print the tack trace on timeout
            if not em.isPropertySet('MaxTimeoutCount'):
                em.MaxTimeoutCount = 1  # abort the stalled event on the first timeout
        except (ImportError, AttributeError):
            # if StalledEventMonitor does not exist or it's an old version, the
            # partial configuration achieved is good enough
            pass

    def stopOnSignal(self):
        '''
        Allow the application to stop cleanly when a signal is received (e.g.
        SIGINT or SIGXCPU).
        '''
        try:
            from Configurables import ApplicationMgr
            app = ApplicationMgr()
            app.StopOnSignal = True  # enable the signal handling
            from Configurables import Gaudi__Utils__StopSignalHandler as StopSignalHandler
            ssh = StopSignalHandler()
            ssh.Signals = ['SIGINT', 'SIGXCPU']
            if self.Application == 'Gauss':
                ssh.Signals.append('SIGUSR1')
                from Configurables import XMLSummarySvc
                XMLSummarySvc('CounterSummarySvc').SuccessExitCodes = [
                    138  # exit code for SIGUSR1
                ]
        except (ImportError, AttributeError):
            # If the application is very old and the StopOnSignal or
            # SuccessExitCodes properties are not available,
            # we can ignore the failure.
            pass

    def __apply_configuration__(self):
        """
        Translate the high level options to the low level configuration.
        """
        logging.getLogger("ProdConf").info("ProdConf version %s", __version__)
        if not self.isPropertySet("Application"):
            raise ValueError("Property Application has not been set")
        # Prepare a dictionary with the values of the properties
        # (used in the string templates)
        propDict = dict(
            [(name, self.getProp(name)) for name in self.__slots__])
        # common configuration
        from Configurables import LHCbApp, HistogramPersistencySvc
        from Gaudi.Configuration import FileCatalog
        app = LHCbApp()

        # Propagate options to LHCbApp, if set.
        for src, dst in [("NOfEvents", "EvtMax"), ("DDDBTag", "DDDBtag"),
                         ("CondDBTag", "CondDBtag"), ("DQTag", "DQFLAGStag")]:
            if self.isPropertySet(src):
                setattr(app, dst, self.getProp(src))

        app.TimeStamp = True

        app.XMLSummary = self.getProp("XMLSummaryFile") % propDict

        if self.isPropertySet("HistogramFile"):
            HistogramPersistencySvc(OutputFile=self.HistogramFile % propDict)

        FileCatalog(Catalogs=[
            "xmlcatalog_file:" + (self.getProp("XMLFileCatalog") % propDict)
        ])

        def dressInputFileNames(name):
            """Small helper to decorate the input file names."""
            if name.endswith(".raw"):
                return "DATAFILE='%s' SVC='LHCb::MDFSelector'" % name
            return "DATAFILE='%s' TYP='POOL_ROOTTREE' OPT='READ'" % name

        if self.isPropertySet("InputFiles"):
            from Configurables import EventSelector
            EventSelector().Input = [
                dressInputFileNames(f) for f in self.InputFiles
            ]

        majorVersion = self._appVersion()[0]
        if self.Application == 'Gauss' and majorVersion == 60:
            pass
        else:
            self.abortStalledEvents()
            self.stopOnSignal()

        # call application-specific configuration (if it exists)
        fn = "_" + self.Application
        if hasattr(self, fn):
            getattr(self, fn)()

        # Workaround for GAUDI-1304
        for c in self._enabledConfUsers:
            c._enabled = True


# The configure function is just an alias to the ProdConf configurable
configure = ProdConf
