###############################################################################
# (c) Copyright 2012-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
'''
Module to host Working Groups specific production settings.
'''

from GaudiKernel.Configurable import Configurable, ConfigurableUser
from Gaudi.Configuration import allConfigurables

import Configurables

__all__ = ['WGProd']


def _WGProd_used_confs():
    '''
    Generate the list of configurables WGProd may use.
    '''
    confs = []
    if hasattr(Configurables, 'DaVinci'):
        confs.append(Configurables.DaVinci)
    if hasattr(Configurables, 'SelDSTWriter'):
        # special case to avoid crashed on old versions of DaVinci, where
        # MyDSTWriter is not a Configurables.SelDSTWriter
        allConf = Configurable.allConfigurables
        if 'MyDSTWriter' not in allConf:
            confs.append((Configurables.SelDSTWriter, 'MyDSTWriter'))
        elif isinstance(allConf['MyDSTWriter'], Configurables.SelDSTWriter):
            confs.append((type(allConf['MyDSTWriter']), 'MyDSTWriter'))
    return confs


class WGProd(ConfigurableUser):
    '''
    Control production settings for WG productions.
    '''
    # yapf: disable
    # Properties of the configurable.
    __slots__ = {'OutputFilePrefix': '',
                 'OutputFileTypes': []}

    # Doc-strings for the properties.
    _propertyDocDct = {'OutputFilePrefix':
                           'name of the prefix of the output file, defined by '
                           'the production',
                       'OutputFileTypes':
                           'extensions (without ".") of the output files to '
                           'be produced, as specified by the step'
                      }
    # yapf: enable
    __used_configurables__ = _WGProd_used_confs()

    def __apply_configuration__(self):
        '''
        Translate the high level options to the low level configuration.
        '''

        # propagates the histogram name from the histogram service to the
        # application
        from Configurables import HistogramPersistencySvc
        hs = HistogramPersistencySvc()

        if hasattr(hs, "OutputFile"):
            Configurables.DaVinci(HistogramFile=hs.OutputFile)

        # Loop over filetypes. Only one .root file is expected and is used
        # as TupleFile
        isTupleSet = False
        for filetype in self.OutputFileTypes:
            if filetype[-4:].lower() == 'root':
                if isTupleSet == False:
                    Configurables.DaVinci(
                        TupleFile=self.OutputFilePrefix + "." + filetype)
                    isTupleSet = True
                else:
                    raise RuntimeError(
                        'WGProd.OutputFileTypes may contain at most one root file, %s provided.'
                        % self.OutputFileTypes)

            elif filetype[-3:].lower() == 'dst':
                writer = Configurables.SelDSTWriter('MyDSTWriter')
                writer.OutputFileSuffix = self.OutputFilePrefix

            else:
                raise RuntimeError(
                    'WGProd.OutputFileTypes: Unknown filetype %s ' %
                    (filetype))


if 'WGProd' in Configurables.__all__:
    # override the local implementation with one coming from the
    # application environment
    from Configurables import WGProd
else:
    # the local implementation is the only one, so we add it to Configurables
    Configurables.WGProd = WGProd
