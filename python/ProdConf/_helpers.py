###############################################################################
# (c) Copyright 2012-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Helper functions used by ProdConf.
"""
__author__ = "Marco Clemencic <marco.clemencic@cern.ch>"


def parseSubversionKeywords(revision, url):
    """
    Helper function to parse the Subversion keywords 'Revision' and 'URL' and
    translate then in a proper version string.

    The returned version string format depends on the type of version:

    >>> parseSubversionKeywords('$Revision: 12345 $', '$URL: http://repos/trunk/ProdConf/python... $')
    'trunk@12345'

    >>> parseSubversionKeywords('$Revision: 12345 $', '$URL: http://repos/tags/ProdConf/v1r7/python... $')
    'v1r7'

    >>> parseSubversionKeywords('$Revision: 12345 $', '$URL: http://repos/branches/ProdConf/v1b/python... $')
    'v1b@12345'

    >>> parseSubversionKeywords('$Revision$', '$URL: http://repos/trunk/ProdConf/python... $')
    'unknown'

    >>> parseSubversionKeywords('$Revision: 12345 $', '$URL$')
    'unknown'
    """
    import re
    revision = int(re.search(r"Revision\D*(\d*)", revision).group(1) or -1)
    url = url.split('/')

    version = "unknown"
    if revision > 0:
        if "trunk" in url:
            version = "trunk@%d" % revision
        elif "tags" in url:
            version = url[url.index("tags") + 2]
        elif "branches" in url:
            version = url[url.index("branches") + 2] + "@%d" % revision
    return version
