====================
ProdConf Development
====================

Prepare a working environment
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

To prepare a working directory, follow the steps:

.. code-block:: bash

    mkdir workspace
    cd workspace
    lb-set-workspace .
    git lb-clone-pkg ProdConf
    cd ProdCond
    git checkout -b ${USER}-work-branch
    cd ..

Then you need an example of how the ``ProdConf`` instance will be configured in the poduction job.
You should have a file called ``prodConf.py``  looking a bit like::

    from ProdConf import ProdConf

    ProdConf(
        NOfEvents=-1,
        DDDBTag='dddb-20170721-3',
        CondDBTag='sim-20161124-vc-md100',
        AppVersion='v39r1',
        XMLSummaryFile='summaryDaVinci_00104077_00000010_7.xml',
        Application='DaVinci',
        OutputFilePrefix='00104077_00000010_7',
        XMLFileCatalog='pool_xml_catalog.xml',
        InputFiles=['LFN:00104077_00000010_6.AllStreams.mdst'],
        OutputFileTypes=['dstard02hhhh.hltfilter.mdst'],
    )

The last bit of information is the command line used to run the application in
production, for example:

.. code-block:: bash

    lb-run --use AppConfig.v3r394 --use WG/CharmConfig.v3r98 --use ProdConf DaVinci/v39r1 \
        gaudirun.py -T \$CHARMCONFIGOPTS/MCFiltering/DstarD02hhhhTriggerFiltering.py \
            \$APPCONFIGOPTS/DaVinci/DataType-2015.py \
            \$APPCONFIGOPTS/Persistency/Compression-ZLIB-1.py \
            prodConf.py

It's probably worth creating an alias for the command, minding to add a ``-n`` (dry-run) to
the ``gaudirun.py`` arguments.

.. code-block:: bash

    alias test-run="lb-run --use AppConfig.v3r394 --use WG/CharmConfig.v3r98 --use ProdConf DaVinci/v39r1 \
        gaudirun.py -T '\$CHARMCONFIGOPTS/MCFiltering/DstarD02hhhhTriggerFiltering.py' \
            '\$APPCONFIGOPTS/DaVinci/DataType-2015.py' \
            '\$APPCONFIGOPTS/Persistency/Compression-ZLIB-1.py' \
            prodConf.py -n"


**Note**: be careful about quoting environment variables in the alias.

At this point you can run the command:

.. code-block:: bash

    test-run -o config-output.py

and examime the configuration output in ``config-output.py``.
