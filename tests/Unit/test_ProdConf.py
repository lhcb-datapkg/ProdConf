###############################################################################
# (c) Copyright 2012-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import unittest

# prepend mock python modules directory to the modules path
import os
import sys
sys.path.insert(0, os.path.join(os.path.dirname(__file__), "python"))

from GaudiKernel.Configurable import purge as purgeConf, applyConfigurableUsers
import GaudiKernel.Configurable  #@UnusedImport

import Configurables
import GaudiConf
import Swimming.Configuration

import logging
logging.getLogger().setLevel(logging.NOTSET)


class RecordHandler(logging.Handler):
    def __init__(self, level=logging.NOTSET):
        logging.Handler.__init__(self, level=level)
        self.records = []

    def filter(self, record):  #@ReservedAssignment
        self.records.append(record)
        return False

    def emit(self, record):
        pass  # ignore


from ProdConf import ProdConf

# Record log messages of the ProdConf logger
prodConfLogRecHandler = RecordHandler()
logging.getLogger("ProdConf").addHandler(prodConfLogRecHandler)

# Record log messages of the Configurable logger (used to check the order of
# __apply_configuration__)
confLogRecHandler = RecordHandler()
logging.getLogger("Configurable").addHandler(confLogRecHandler)
logging.getLogger("TestConfigurableUser").addHandler(confLogRecHandler)


class ProdConfTest(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        # Reset logging records
        confLogRecHandler.records = []
        prodConfLogRecHandler.records = []
        if "KaliCalo.Configuration" in sys.modules:
            # restore the normal KaliCalo module
            del sys.modules["KaliCalo.Configuration"]

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        purgeConf()
        GaudiKernel.Configurable._appliedConfigurableUsers_ = False
        # Unload the modules DSTWriters and DSTWriters.__dev__ to be able to
        # test different cases (with or without the submodule __dev___).
        for n in [n for n in sys.modules if n.startswith("DSTWriters")]:
            del sys.modules[n]

    def checkApplyOrder(self):
        """
        Helper function to ensure that the order of the calls to
        __apply_configuration__ is correct.
        """
        # make a list of strings from the records
        msgs = [
            r.msg % r.args for r in confLogRecHandler.records
            if type(r.msg) is str
        ]
        self.assert_(msgs, "no logged messages")
        # ProdConf must be the first one
        self.assertEquals(msgs[0], "applying configuration of ProdConf")
        # check if the application has been applied
        appName = ProdConf().Application
        if appName == "LHCb":
            appName = "LHCbApp"
        msgs.index("applying configuration of %s" % appName)

    def test_checkPropValue(self):
        self.assertRaises(ValueError, ProdConf, Application="unknown")
        ProdConf(Application="Brunel")

    def test_version_decoding(self):
        # pylint: disable=W0212
        pc = ProdConf()
        pc.AppVersion = "v12r34"
        self.failUnless(pc._appVersion() == (12, 34, 0, 0))
        pc.AppVersion = "v9r8p1"
        self.failUnless(pc._appVersion() == (9, 8, 1, 0))
        pc.AppVersion = "v1r12p123g1234"
        self.failUnless(pc._appVersion() == (1, 12, 123, 1234))
        pc.AppVersion = "v1r1g1"
        self.failUnless(pc._appVersion() == (1, 1, 0, 1))
        pc.AppVersion = "test"
        self.failUnless(pc._appVersion() == None)
        pc.AppVersion = "v1"
        self.failUnless(pc._appVersion() == None)
        pc.AppVersion = "v010r0"
        self.failUnless(pc._appVersion() == (10, 0, 0, 0))

    def test_applyConfUser1(self):
        ProdConf(
            Application="DaVinci",
            OutputFilePrefix="output",
            OutputFileTypes=["file"],
            DDDBTag="test",
            CondDBTag="test",
            InputFiles=["PFN:input.file"])
        applyConfigurableUsers()
        self.failUnless(Configurables.DaVinci().applied)

    def test_applyConfUser2(self):
        ProdConf(
            Application="Gauss",
            OutputFilePrefix="output",
            OutputFileTypes=["file"],
            DDDBTag="test",
            CondDBTag="test",
            RunNumber=1,
            FirstEventNumber=1,
            NOfEvents=10,
            HistogramFile="histos.root")
        applyConfigurableUsers()
        self.failUnless(Configurables.LHCbApp().EvtMax == 10)

    def test_applyConfUser3(self):
        ProdConf(
            Application="Boole",
            OutputFilePrefix="output",
            OutputFileTypes=["file"],
            DDDBTag="test",
            CondDBTag="test",
            RunNumber=1,
            FirstEventNumber=1,
            NOfEvents=10,
            HistogramFile="histos.root",
            InputFiles=["PFN:input1.file", "LFN:input2.file"])
        applyConfigurableUsers()
        self.failUnless(len(Configurables.EventSelector().Input) == 2)

    def test_appChange(self):
        ProdConf(Application="DaVinci")
        ProdConf(Application="DaVinci")
        ProdConf(Application="Brunel")
        warns = [
            r.msg % r.args for r in prodConfLogRecHandler.records
            if r.levelno >= logging.WARNING
        ]
        self.failUnless((len(warns) == 1)
                        and ("Changing application" in warns[0]))

    def test_ProdConf_version(self):
        ProdConf(
            Application="DaVinci",
            OutputFilePrefix="output",
            OutputFileTypes=["file"],
            DDDBTag="test",
            CondDBTag="test",
            InputFiles=["PFN:input.file"])
        applyConfigurableUsers()
        # Check that the ProdConf version has been printed
        self.failUnless([
            r.msg % r.args for r in prodConfLogRecHandler.records
            if r.msg.startswith("ProdConf version")
        ])

    def test_missingOpt1(self):
        ProdConf(Application="DaVinci")
        applyConfigurableUsers()

    def test_missingOpt2(self):
        ProdConf(OutputFilePrefix="test_prefix")
        self.assertRaises(ValueError, applyConfigurableUsers)

    def test_missingOpt3(self):
        ProdConf(
            Application="Gauss",
            OutputFilePrefix="output",
            OutputFileTypes=["file"],
            DDDBTag="test",
            CondDBTag="test")
        applyConfigurableUsers()

    def test_wrongOpts(self):
        ProdConf(
            Application="Gauss",
            OutputFilePrefix="output",
            OutputFileTypes=["file1", "file2"],
            DDDBTag="test",
            CondDBTag="test",
            RunNumber=1,
            FirstEventNumber=1,
            NOfEvents=10)
        self.assertRaises(ValueError, applyConfigurableUsers)

    def test_noOutputName_1(self):
        ProdConf(
            Application="Gauss",
            OutputFileTypes=["file"],
            DDDBTag="test",
            CondDBTag="test")
        applyConfigurableUsers()

    def test_noOutputName_2(self):
        ProdConf(
            Application="Gauss",
            OutputFilePrefix="output",
            DDDBTag="test",
            CondDBTag="test")
        applyConfigurableUsers()

    def test_confDep1(self):
        dv = Configurables.DaVinci()
        self.failUnless(dv.__users__ == [])
        pc = ProdConf(Application="DaVinci")
        self.failUnless(pc in dv.__users__)

    def test_confDep2(self):
        la = Configurables.LHCbApp()
        self.failUnless(la.__users__ == [])
        pc = ProdConf(Application="LHCb")
        self.failUnless(pc in la.__users__)

    def test_app_Moore_1(self):
        ProdConf(
            Application="Moore",
            OutputFilePrefix="output",
            OutputFileTypes=["digi"],
            HistogramFile="%(Application)s_%(OutputFilePrefix)s_Hist.root",
            DDDBTag="test",
            CondDBTag="test",
            RunNumber=1,
            InputFiles=["PFN:input1.file", "LFN:input2.file"])
        la = Configurables.LHCbApp()
        hs = Configurables.HistogramPersistencySvc()
        l0 = Configurables.L0App(_enabled=False)
        mo = Configurables.Moore(_enabled=False)
        applyConfigurableUsers()
        self.failUnless(mo._enabled is True)
        self.failUnless(l0._enabled is False)
        self.failUnless(la.XMLSummary == 'summaryMoore_output.xml')
        self.failUnless(hs.OutputFile == 'Moore_output_Hist.root')

    def test_app_Moore_2(self):
        ProdConf(
            Application="Moore",
            OptionFormat="Swimming2011",
            OutputFilePrefix="output",
            OutputFileTypes=["charmtriggerswimming.dst"],
            HistogramFile="%(Application)s_%(OutputFilePrefix)s_Hist.root",
            DDDBTag="test",
            CondDBTag="test",
            RunNumber=1,
            InputFiles=["PFN:input1.file", "LFN:input2.file"],
            NOfEvents=321,
        )
        mo = Configurables.Moore()
        sw = Configurables.Swimming()
        applyConfigurableUsers()
        self.failUnless(mo.outputFile == 'output.charmtriggerswimming.dst')
        self.failUnless(sw.OutputFile == 'output.charmtriggerswimming.dst')
        self.failUnless(sw.XMLSummary == 'summaryMoore_output.xml')
        self.failUnless(sw.EvtMax == 321)
        self.failUnless(sw.RunNumber == 1)
        self.failUnless(Swimming.Configuration.called)

    def test_app_Moore_3(self):
        ProdConf(
            Application="Moore",
            OptionFormat="Swimming2012",
            OutputFilePrefix="output",
            OutputFileTypes=["charmtriggerswimming.dst"],
            HistogramFile="%(Application)s_%(OutputFilePrefix)s_Hist.root",
            DDDBTag="test_d",
            CondDBTag="test_c",
            TCK="abc",
            InputFiles=["PFN:input1.file", "LFN:input2.file"],
            NOfEvents=425,
            XMLSummaryFile="Swim2012.xml")
        mo = Configurables.Moore()
        sw = Configurables.Swimming()
        applyConfigurableUsers()
        self.failUnless(mo.outputFile == 'output.charmtriggerswimming.dst')
        self.failUnless(sw.OutputFile == 'output.charmtriggerswimming.dst')
        self.failUnless(sw.XMLSummary == 'Swim2012.xml')
        self.failUnless(sw.EvtMax == 425)
        self.failUnless(sw.TCK == "abc")
        self.failUnless(sw.CondDBtag == "test_c")
        self.failUnless(sw.DDDBtag == "test_d")
        self.failUnless(Swimming.Configuration.called == "Moore")

    def test_app_Moore_4(self):
        ProdConf(
            Application="Moore",
            OptionFormat="l0app",
            OutputFilePrefix="output",
            OutputFileTypes=["digi"],
            HistogramFile="%(Application)s_%(OutputFilePrefix)s_Hist.root",
            DDDBTag="test",
            CondDBTag="test",
            RunNumber=1,
            InputFiles=["PFN:input1.file", "LFN:input2.file"])
        la = Configurables.LHCbApp()
        l0 = Configurables.L0App(_enabled=False)
        mo = Configurables.Moore(_enabled=False)
        hs = Configurables.HistogramPersistencySvc()
        applyConfigurableUsers()
        self.failUnless(mo._enabled is False)
        self.failUnless(l0._enabled is True)
        self.failUnless(la.XMLSummary == 'summaryMoore_output.xml')
        self.failUnless(hs.OutputFile == 'Moore_output_Hist.root')
        self.failUnless(l0.outputFile == 'output.digi')

    def test_app_Brunel_1(self):
        ProdConf(
            Application="Brunel",
            OutputFilePrefix="output",
            OutputFileTypes=["sdst"],
            HistogramFile="%(Application)s_%(OutputFilePrefix)s_Hist.root",
            DDDBTag="test",
            CondDBTag="test",
            RunNumber=1,
            InputFiles=["LFN:input1.raw", "PFN:input2.digi"])
        la = Configurables.LHCbApp()
        es = Configurables.EventSelector()
        br = Configurables.Brunel(_enabled=False)
        applyConfigurableUsers()
        self.failUnless(br._enabled is True)
        self.failUnless(la.XMLSummary == 'summaryBrunel_output.xml')
        self.failUnless(br.OutputType == "SDST")
        self.failUnless('MDFSelector' in es.Input[0])
        self.failUnless('POOL_ROOTTREE' in es.Input[1])

    def test_app_Brunel_2(self):
        ProdConf(
            Application="Brunel",
            OutputFilePrefix="output",
            OutputFileTypes=["full.dst"],
            HistogramFile="%(Application)s_%(OutputFilePrefix)s_Hist.root",
            DDDBTag="test",
            CondDBTag="test",
            RunNumber=1,
            InputFiles=["LFN:input1.raw", "PFN:input2.digi"])
        la = Configurables.LHCbApp()
        es = Configurables.EventSelector()
        br = Configurables.Brunel()
        applyConfigurableUsers()
        self.failUnless(la.XMLSummary == 'summaryBrunel_output.xml')
        self.failUnless(br.OutputType == "DST")
        self.failUnless('MDFSelector' in es.Input[0])
        self.failUnless('POOL_ROOTTREE' in es.Input[1])

    def test_app_LHCb_1(self):
        ProdConf(
            Application="LHCb",
            OutputFilePrefix="output",
            OutputFileTypes=["allstreams.dst"],
            HistogramFile="%(Application)s_%(OutputFilePrefix)s_Hist.root",
            XMLSummaryFile="summaryMerge_%(OutputFilePrefix)s.xml",
            DDDBTag="",
            CondDBTag="",
            RunNumber=1,
            InputFiles=["PFN:input1.dst", "LFN:input2.dst"])
        la = Configurables.LHCbApp(_enabled=False)
        am = Configurables.ApplicationMgr()
        applyConfigurableUsers()
        self.failUnless(la._enabled is True)
        self.failUnless(la.XMLSummary == 'summaryMerge_output.xml')
        self.failUnless(am.TopAlg == [Configurables.LbAppInit()])

    def test_app_LHCb_2(self):
        # same as test_app_LHCb_1, but in the case InputCopyStream is not available
        ICS = Configurables.InputCopyStream
        del Configurables.InputCopyStream
        self.test_app_LHCb_1()
        Configurables.InputCopyStream = ICS  # restore the class in the module

    def test_app_LHCb_3(self):
        ProdConf(
            Application="LHCb",
            OptionFormat="indexer",
            OutputFilePrefix="output",
            OutputFileTypes=["root"],
            XMLSummaryFile="summaryIndexer_%(OutputFilePrefix)s.xml",
            DDDBTag="",
            CondDBTag="",
            RunNumber=1,
            InputFiles=["PFN:input1.dst", "LFN:input2.dst"],
            ProcessingPass="Stripping20")
        la = Configurables.LHCbApp()
        #am = Configurables.ApplicationMgr()
        ei = Configurables.EventIndexer()
        applyConfigurableUsers()
        self.failUnless(la.XMLSummary == 'summaryIndexer_output.xml')
        self.failUnless(ei.OutputFile == 'output.root')
        self.failUnless(ei.Stripping == 'Stripping20')
        #self.failUnless(am.TopAlg == [Configurables.LbAppInit(), ei])

    def test_app_Noether_1(self):
        ProdConf(
            Application="Noether",
            OptionFormat="indexer",
            OutputFilePrefix="output",
            OutputFileTypes=["root"],
            XMLSummaryFile="summaryIndexer_%(OutputFilePrefix)s.xml",
            DDDBTag="",
            CondDBTag="",
            RunNumber=1,
            InputFiles=["PFN:input1.dst", "LFN:input2.dst"],
            ProcessingPass="Stripping20")
        la = Configurables.LHCbApp(_enabled=False)
        am = Configurables.ApplicationMgr()
        ei = Configurables.EventIndexer()
        am.TopAlg.append(ei)
        applyConfigurableUsers()
        self.failUnless(la._enabled is True)
        self.failUnless(la.XMLSummary == 'summaryIndexer_output.xml')
        self.failUnless(ei.OutputFile == 'output.root')
        self.failUnless(ei.Stripping == 'Stripping20')
        self.failUnless(am.TopAlg == [Configurables.LbAppInit(), ei])

    def test_app_Noether_2(self):
        ProdConf(
            Application="Noether",
            OptionFormat="RawEventJuggler",
            OutputFilePrefix="output",
            OutputFileTypes=["dst"],
            XMLSummaryFile="summaryRawEventJuggler_%(OutputFilePrefix)s.xml",
            DDDBTag="",
            CondDBTag="",
            RunNumber=1,
            InputFiles=["PFN:input1.dst", "LFN:input2.dst"])
        la = Configurables.LHCbApp(_enabled=False)
        am = Configurables.ApplicationMgr()
        rj = Configurables.RawEventJuggler()
        applyConfigurableUsers()
        self.failUnless(rj.WriterOptItemList is not None)
        self.failUnless(rj.WriterOptItemList == GaudiConf.mywriter)

    def test_app_DaVinci_1a(self):
        # see task #30221 https://savannah.cern.ch/task/?30221
        import KaliCalo.Configuration as kcc
        kcc.action_called = False  # ensure we can check the action_called flag

        dw = Configurables.SelDSTWriter("MyDSTWriter")
        dv = Configurables.DaVinci(_enabled=False)
        ProdConf(
            Application="DaVinci",
            OutputFilePrefix="output",
            OutputFileTypes=["allstreams.dst"],
            HistogramFile="%(Application)s_%(OutputFilePrefix)s_Hist.root",
            DDDBTag="test",
            CondDBTag="test",
            RunNumber=1,
            NOfEvents=10,
            InputFiles=["PFN:input1.dst", "LFN:input2.dst"])
        applyConfigurableUsers()
        self.failUnless(dv._enabled is True)
        self.failUnless(dw.OutputFileSuffix == 'output')
        self.failUnless(dv.HistogramFile == "DaVinci_output_Hist.root")
        self.failUnless(dv.EvtMax == 10)
        self.checkApplyOrder()

        self.failUnless(kcc.action_called == False,
                        "Post-config action called")

    def test_app_DaVinci_1b(self):
        ProdConf(
            Application="DaVinci",
            OutputFilePrefix="output",
            OutputFileTypes=["allstreams.dst"],
            HistogramFile="%(Application)s_%(OutputFilePrefix)s_Hist.root",
            DDDBTag="test",
            CondDBTag="test",
            RunNumber=1,
            NOfEvents=10,
            InputFiles=["PFN:input1.dst", "LFN:input2.dst"])
        dw = Configurables.SelDSTWriter("MyDSTWriter")
        dv = Configurables.DaVinci()
        applyConfigurableUsers()
        self.failUnless(dw.OutputFileSuffix == 'output')
        self.failUnless(dv.HistogramFile == "DaVinci_output_Hist.root")
        self.failUnless(dv.EvtMax == 10)
        self.checkApplyOrder()

    def test_app_DaVinci_1c(self):
        # Enable importing of DSTWriters from the __dev__submodule
        import DSTWriters
        DSTWriters.has_dev = True

        ProdConf(
            Application="DaVinci",
            OutputFilePrefix="output",
            OutputFileTypes=["allstreams.dst"],
            HistogramFile="%(Application)s_%(OutputFilePrefix)s_Hist.root",
            DDDBTag="test",
            CondDBTag="test",
            RunNumber=1,
            NOfEvents=10,
            InputFiles=["PFN:input1.dst", "LFN:input2.dst"])
        dw = Configurables.SelDSTWriter("MyDSTWriter")
        dv = Configurables.DaVinci()
        applyConfigurableUsers()
        self.failUnless(dw.OutputFileSuffix == 'output')
        self.failUnless(dv.HistogramFile == "DaVinci_output_Hist.root")
        self.failUnless(dv.EvtMax == 10)
        self.checkApplyOrder()

    def test_app_DaVinci_2(self):
        ProdConf(
            Application="DaVinci",
            OptionFormat="DQ",
            OutputFilePrefix="output",
            OutputFileTypes=["DST"],
            HistogramFile="%(Application)s_%(OutputFilePrefix)s_Hist.root",
            DDDBTag="test",
            CondDBTag="test",
            RunNumber=1,
            InputFiles=["PFN:input1.dst", "LFN:input2.dst"])
        dv = Configurables.DaVinci()
        ics = Configurables.InputCopyStream()
        applyConfigurableUsers()
        self.failUnless(ics in dv.MoniSequence)
        self.failUnless("output.dst" in ics.Output)

    def test_app_DaVinci_3(self):
        ProdConf(
            Application="DaVinci",
            OptionFormat="merge",
            OutputFilePrefix="output",
            OutputFileTypes=["DST"],
            HistogramFile="%(Application)s_%(OutputFilePrefix)s_Hist.root",
            DDDBTag="test",
            CondDBTag="test",
            RunNumber=1,
            InputFiles=["PFN:input1.dst", "LFN:input2.dst"])
        ics = Configurables.InputCopyStream()
        fr = Configurables.RecordStream("FileRecords")
        applyConfigurableUsers()
        self.failUnless("output.dst" in ics.Output)
        self.failUnless("output.dst" in fr.Output)

    def test_app_DaVinci_4a(self):
        ProdConf(
            Application="DaVinci",
            OptionFormat="swimming2011",
            OutputFilePrefix="output",
            OutputFileTypes=["charmstrippingswimming.mdst"],
            HistogramFile="%(Application)s_%(OutputFilePrefix)s_Hist.root",
            DDDBTag="test_d",
            CondDBTag="test_c",
            RunNumber=1,
            InputFiles=["PFN:input1.dst", "LFN:input2.dst"],
            NOfEvents=321,
        )
        sw = Configurables.Swimming()
        applyConfigurableUsers()
        self.failUnless(sw.OutputFile == "output.charmstrippingswimming.mdst")
        self.failUnless(sw.XMLSummary == 'summaryDaVinci_output.xml')
        self.failUnless(sw.EvtMax == 321)
        self.failUnless(sw.CondDBtag == "test_c")
        self.failUnless(sw.DDDBtag == "test_d")
        self.failUnless(Swimming.Configuration.called == "DaVinci")

    def test_app_DaVinci_4b(self):
        ProdConf(
            Application="DaVinci",
            OptionFormat="swimming2012",
            OutputFilePrefix="output",
            OutputFileTypes=["charmstrippingswimming.mdst"],
            HistogramFile="%(Application)s_%(OutputFilePrefix)s_Hist.root",
            DDDBTag="test_d",
            CondDBTag="test_c",
            RunNumber=1,
            InputFiles=["PFN:input1.dst", "LFN:input2.dst"],
            NOfEvents=425,
            XMLSummaryFile="DVSwim2012.xml")
        sw = Configurables.Swimming()
        applyConfigurableUsers()
        self.failUnless(sw.OutputFile == "output.charmstrippingswimming.mdst")
        self.failUnless(sw.XMLSummary == 'DVSwim2012.xml')
        self.failUnless(sw.EvtMax == 425)
        self.failUnless(sw.CondDBtag == "test_c")
        self.failUnless(sw.DDDBtag == "test_d")
        self.failUnless(Swimming.Configuration.called == "DaVinci")

    def test_app_DaVinci_4c(self):
        # check the case of versions of DaVinci without Swimming
        s = Configurables.Swimming
        del Configurables.Swimming
        try:
            ProdConf(
                Application="DaVinci",
                OutputFilePrefix="output",
                OutputFileTypes=["allstreams.dst"],
                HistogramFile="%(Application)s_%(OutputFilePrefix)s_Hist.root",
                DDDBTag="test",
                CondDBTag="test",
                RunNumber=1,
                NOfEvents=10,
                InputFiles=["PFN:input1.dst", "LFN:input2.dst"])
            dw = Configurables.SelDSTWriter("MyDSTWriter")
            dv = Configurables.DaVinci()
            applyConfigurableUsers()
            self.failUnless(dw.OutputFileSuffix == 'output')
            self.failUnless(dv.HistogramFile == "DaVinci_output_Hist.root")
            self.failUnless(dv.EvtMax == 10)
            self.checkApplyOrder()
        finally:
            Configurables.Swimming = s

    def test_app_DaVinci_5(self):
        # see task #30221 https://savannah.cern.ch/task/?30221
        import KaliCalo.Configuration as kcc
        kcc.action_called = False  # ensure we can check the action_called flag

        ProdConf(
            Application="DaVinci",
            OptionFormat="femto",
            OutputFilePrefix="output",
            OutputFileTypes=["fmdst"],
            HistogramFile="%(Application)s_%(OutputFilePrefix)s_Hist.root",
            DDDBTag="test",
            CondDBTag="test",
            RunNumber=1,
            InputFiles=["PFN:input1.dst", "LFN:input2.dst"])
        kpc = Configurables.KaliPi0Conf()
        applyConfigurableUsers()
        self.failUnless(kpc.FemtoDST == "output.fmdst")
        self.failUnless(kpc.OutputLevel == 5)
        self.failUnless(kpc.PrintFreq == 10000)
        self.checkApplyOrder()

        self.failUnless(kcc.action_called == True,
                        "Post-config action not called")

    def test_app_DaVinci_6(self):
        # remove the bad code and it's effects in KaliCalo.Configuration
        # see task #30221 https://savannah.cern.ch/task/?30221
        import KaliCalo.Configuration as kcc
        # equivalent to the fix in Analysis > v10r0
        # see https://svnweb.cern.ch/trac/lhcb/changeset/142560
        GaudiKernel.Configurable.removePostConfigAction(kcc.action)

        kcc.action_called = False  # ensure we can check the action_called flag

        ProdConf(
            Application="DaVinci",
            OptionFormat="femto",
            OutputFilePrefix="output",
            OutputFileTypes=["fmdst"],
            HistogramFile="%(Application)s_%(OutputFilePrefix)s_Hist.root",
            DDDBTag="test",
            CondDBTag="test",
            RunNumber=1,
            InputFiles=["PFN:input1.dst", "LFN:input2.dst"])
        kpc = Configurables.KaliPi0Conf()
        applyConfigurableUsers()
        self.failUnless(kpc.FemtoDST == "output.fmdst")
        self.failUnless(kpc.OutputLevel == 5)
        self.failUnless(kpc.PrintFreq == 10000)
        self.checkApplyOrder()

        self.failUnless(kcc.action_called == False,
                        "Post-config action called")

    def test_abortStalledEvents_1(self):
        # standard
        pc = ProdConf()
        pc.abortStalledEvents()

        app = Configurables.ApplicationMgr()
        em = Configurables.StalledEventMonitor()

        self.failUnless(app.StalledEventMonitoring == True)
        self.failUnless(em.EventTimeout == 3600)
        self.failUnless(em.StackTrace == True)
        self.failUnless(em.MaxTimeoutCount == 1)

    def test_abortStalledEvents_2(self):
        # custom timeout
        pc = ProdConf()
        pc.abortStalledEvents(1800)

        app = Configurables.ApplicationMgr()
        em = Configurables.StalledEventMonitor()

        self.failUnless(app.StalledEventMonitoring == True)
        self.failUnless(em.EventTimeout == 1800)
        self.failUnless(em.StackTrace == True)
        self.failUnless(em.MaxTimeoutCount == 1)

    def test_abortStalledEvents_3(self):
        # predefined max timeout count
        app = Configurables.ApplicationMgr()
        em = Configurables.StalledEventMonitor()
        em.EventTimeout = 120
        em.MaxTimeoutCount = 0

        pc = ProdConf()
        pc.abortStalledEvents()

        self.failUnless(app.StalledEventMonitoring == True)
        self.failUnless(em.EventTimeout == 120)
        self.failUnless(em.StackTrace == True)
        self.failUnless(em.MaxTimeoutCount == 0)

    def test_abortStalledEvents_4(self):
        # old StalledEventMonitor
        sem_bk = Configurables.StalledEventMonitor
        Configurables.StalledEventMonitor = Configurables.StalledEventMonitorOld

        pc = ProdConf()
        pc.abortStalledEvents()

        app = Configurables.ApplicationMgr()
        em = Configurables.StalledEventMonitor()

        self.failUnless(app.StalledEventMonitoring == True)
        self.failUnless(em.EventTimeout == 3600)

        Configurables.StalledEventMonitor = sem_bk

    def test_abortStalledEvents_5(self):
        # apply configuration
        ProdConf(
            Application="LHCb",
            OutputFilePrefix="output",
            OutputFileTypes=["allstreams.dst"],
            HistogramFile="%(Application)s_%(OutputFilePrefix)s_Hist.root",
            XMLSummaryFile="summaryMerge_%(OutputFilePrefix)s.xml",
            DDDBTag="",
            CondDBTag="",
            RunNumber=1,
            InputFiles=["PFN:input1.dst", "LFN:input2.dst"])

        app = Configurables.ApplicationMgr()
        em = Configurables.StalledEventMonitor()

        applyConfigurableUsers()

        self.failUnless(app.StalledEventMonitoring == True)
        self.failUnless(em.EventTimeout == 3600)
        self.failUnless(em.StackTrace == True)
        self.failUnless(em.MaxTimeoutCount == 1)

    def test_stopOnSignal_1(self):
        # standard
        pc = ProdConf()
        pc.stopOnSignal()

        app = Configurables.ApplicationMgr()

        self.failUnless(app.StopOnSignal == True)

    def test_stopOnSignal_2(self):
        # apply configuration
        ProdConf(
            Application="LHCb",
            OutputFilePrefix="output",
            OutputFileTypes=["allstreams.dst"],
            HistogramFile="%(Application)s_%(OutputFilePrefix)s_Hist.root",
            XMLSummaryFile="summaryMerge_%(OutputFilePrefix)s.xml",
            DDDBTag="",
            CondDBTag="",
            RunNumber=1,
            InputFiles=["PFN:input1.dst", "LFN:input2.dst"])

        app = Configurables.ApplicationMgr()

        applyConfigurableUsers()

        self.failUnless(app.StopOnSignal == True)


if __name__ == '__main__':
    unittest.main()
