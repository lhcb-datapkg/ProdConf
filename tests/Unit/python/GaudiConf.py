###############################################################################
# (c) Copyright 2012-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Mock version of LHCb GaudiConf module.
"""

mywriter = None


class IOHelper(object):
    def outputAlgs(self, filename, writer="OutputStream", writeFSR=True):
        return ['alg1', 'alg2']

    def outStream(self, filename, writer="SomeDefault", writeFSR=True):
        print "Set mywriter = " + ` writer `
        global mywriter
        mywriter = writer
        return None
