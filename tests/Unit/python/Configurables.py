###############################################################################
# (c) Copyright 2012-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Fake configurables for testing.
"""

from GaudiKernel.Configurable import ConfigurableUser
from GaudiKernel.ConfigurableDb import cfgDb

import logging


class TestConfigurableUser(ConfigurableUser):
    __slots__ = {"applied": False}
    log = logging.getLogger("TestConfigurableUser")

    def __apply_configuration__(self):
        # pylint: disable=W0201
        self.log.info("Applying configuration of %s(%r)",
                      self.__class__.__name__, self.name())
        self.applied = True


# applications
class DaVinci(TestConfigurableUser):
    __slots__ = {"MoniSequence": [], "EvtMax": -1, "HistogramFile": ""}


class Brunel(TestConfigurableUser):
    __slots__ = {"ProductionMode": False, "OutputType": "None"}


class Boole(TestConfigurableUser):
    pass


class LHCbApp(TestConfigurableUser):
    __slots__ = {
        "EvtMax": -1,
        "CondDBtag": "",
        "DDDBtag": "",
        "DQFLAGStag": "",
        "XMLSummary": "",
        "TimeStamp": False
    }


class Gauss(TestConfigurableUser):
    pass


class Moore(TestConfigurableUser):
    __slots__ = {
        "CondDBtag": "",
        "DDDBtag": "",
        "outputFile": "",
    }


class L0App(TestConfigurableUser):
    __slots__ = {
        "outputFile": "",
        "TCK": "",
    }


class Swimming(TestConfigurableUser):
    __slots__ = {
        "EvtMax": -1,
        "OutputFile": "",
        "XMLSummary": "",
        "RunNumber": 0,
        "TCK": "",
        "CondDBtag": "",
        "DDDBtag": "",
    }


class KaliPi0Conf(TestConfigurableUser):
    __slots__ = {
        "EvtMax": -1,
        "FemtoDST": "",
        "PrintFreq": 1000,
        "OutputLevel": 3
    }


class SimConf(TestConfigurableUser):
    __slots__ = {}

    def writer(self):
        return OutputStream("Writer")


# services
class ApplicationMgr(TestConfigurableUser):
    __slots__ = {
        "TopAlg": [],
        "StalledEventMonitoring": False,
        "StopOnSignal": False
    }


class HistogramPersistencySvc(TestConfigurableUser):
    __slots__ = {"OutputFile": ""}


class FileCatalog(TestConfigurableUser):
    __slots__ = {"Catalogs": []}


class EventSelector(TestConfigurableUser):
    __slots__ = {"Input": []}


# algorithms
class LbAppInit(TestConfigurableUser):
    pass


class EventIndexer(TestConfigurableUser):
    __slots__ = {"Stripping": "", "OutputFile": ""}


class RawEventJuggler(TestConfigurableUser):
    __slots__ = {"WriterOptItemList": ""}


class GenInit(TestConfigurableUser):
    __slots__ = {"RunNumber": 0, "FirstEventNumber": 0}


class OutputStream(TestConfigurableUser):
    __slots__ = {"Output": ""}


class InputCopyStream(OutputStream):
    pass


class RecordStream(OutputStream):
    pass


class SelDSTWriter(TestConfigurableUser):
    __slots__ = {"OutputFileSuffix": ""}


class MicroDSTWriter(TestConfigurableUser):
    __slots__ = {"OutputFileSuffix": ""}


class StalledEventMonitorOld(TestConfigurableUser):
    __slots__ = {'EventTimeout': 600}


class StalledEventMonitor(TestConfigurableUser):
    __slots__ = {
        'StackTrace': False,
        'MaxTimeoutCount': 0,
        'EventTimeout': 600
    }


class Gaudi__Utils__StopSignalHandler(TestConfigurableUser):
    __slots__ = {'Signals': ['SIGINT', 'SIGXCPU']}


_details = {"package": "Fake", "module": "Configurables", "lib": "Fake"}
for n in ["DaVinci", "Brunel", "LHCbApp", "Gauss", "Boole", "Moore"]:
    cfgDb[n] = _details

__all__ = cfgDb.keys()
