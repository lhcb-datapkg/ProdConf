###############################################################################
# (c) Copyright 2012-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# needed for FemtoDST
from Configurables import KaliPi0Conf  #@UnusedImport

action_called = False


# this is to replicate a bug/feature (bad code) in KaliCalo
# see task #30221 https://savannah.cern.ch/task/?30221
def action():
    print "KaliCalo.Configuration.action called"
    global action_called
    action_called = True


from GaudiKernel.Configurable import appendPostConfigAction
appendPostConfigAction(action)
