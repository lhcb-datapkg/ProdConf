###############################################################################
# (c) Copyright 2012-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from TestTools import ConfigTest


class TestConfig_v23r1(ConfigTest):
    _cmtconfigs = ['i686-slc5-gcc43-opt', 'x86_64-slc5-gcc43-opt']
    application = ("Boole", "v23r1")
    packages = 'AppConfig.v3r118;SQLDDDB.v6r20'
    options = [
        '$APPCONFIGOPTS/Boole/Default.py', '$APPCONFIGOPTS/L0/L0TCK-0x0037.py'
    ]
    settings = {
        'DDDBTag': 'MC11-20111102',
        'CondDBTag': 'sim-20111111-vc-mu100',
        'OutputFilePrefix': '00000000_00000000_0',
        'OutputFileTypes': ['digi'],
        'XMLSummaryFile': 'summaryBoole_00000000_00000000_0.xml',
        'XMLFileCatalog': 'pool_xml_catalog.xml',
        'InputFiles': ["PFN:simulation.digi"],
    }


class TestConfig_v26r3(ConfigTest):
    _cmtconfigs = [
        'x86_64-slc5-gcc43-opt', 'x86_64-slc5-gcc46-opt',
        'x86_64-slc6-gcc46-opt'
    ]
    application = ("Boole", "v26r3")
    packages = 'AppConfig.v3r162;SQLDDDB.v7r9'
    options = [
        '$APPCONFIGOPTS/Boole/Default.py', '$APPCONFIGOPTS/L0/L0TCK-0x0037.py'
    ]
    settings = {
        'DDDBTag': 'MC11-20111102',
        'CondDBTag': 'sim-20111111-vc-mu100',
        'OutputFilePrefix': '00000000_00000000_0',
        'OutputFileTypes': ['digi'],
        'XMLSummaryFile': 'summaryBoole_00000000_00000000_0.xml',
        'XMLFileCatalog': 'pool_xml_catalog.xml',
        'InputFiles': ["PFN:simulation.digi"],
    }
