###############################################################################
# (c) Copyright 2012-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from TestTools import StepTest as ST

# name of the working directory to use
workdir = None


def setup_module():
    global workdir
    # create temporary directory
    from tempfile import mkdtemp
    workdir = mkdtemp('_ProdConfTest')


def teardown_module():
    global workdir
    if workdir:
        import shutil
        shutil.rmtree(workdir, ignore_errors=True)


class StepTest(ST):
    _cmtconfigs = ['i686-slc5-gcc43-opt', 'x86_64-slc5-gcc43-opt']
    prodId = 7294
    jobId = 67890

    @classmethod
    def getWorkdir(cls):
        global workdir
        return workdir


class TestStep1(StepTest):
    application = ("Gauss", "v41r2")
    step = 1
    packages = 'AppConfig.v3r122;DecFiles.v25r4;SQLDDDB.v6r20'
    options = [
        '$APPCONFIGOPTS/Gauss/Beam3500GeV-mu100-MC11-nu2.py',
        '$DECFILESROOT/options/27163005.py', '$LBPYTHIAROOT/options/Pythia.py',
        '$APPCONFIGOPTS/Gauss/G4PL_LHEP_EmNoCuts.py'
    ]
    settings = {
        'DDDBTag': 'MC11-20111102',
        'CondDBTag': 'sim-20111111-vc-mu100',
        'XMLFileCatalog': 'pool_xml_catalog.xml',
        'OutputFileTypes': ['sim'],
        'RunNumber': 1302390,
        'FirstEventNumber': 203668,
        'NOfEvents': 3,
    }


class TestStep2(StepTest):
    application = ("Boole", "v23r1")
    step = 2
    packages = 'AppConfig.v3r118;SQLDDDB.v6r20'
    options = [
        '$APPCONFIGOPTS/Boole/Default.py', '$APPCONFIGOPTS/L0/L0TCK-0x0037.py'
    ]
    settings = {
        'DDDBTag': 'MC11-20111102',
        'CondDBTag': 'sim-20111111-vc-mu100',
        'XMLFileCatalog': 'pool_xml_catalog.xml',
        'OutputFileTypes': ['digi'],
        'InputFiles': ["PFN:" + TestStep1.outputFile()],
    }


class TestStep3(StepTest):
    application = ("Moore", "v12r8g1")
    step = 3
    packages = 'AppConfig.v3r120;SQLDDDB.v6r20'
    options = [
        '$APPCONFIGOPTS/Moore/MooreSimProduction.py',
        '$APPCONFIGOPTS/Conditions/TCK-0x40760037.py',
        '$APPCONFIGOPTS/Moore/DataType-2011.py'
    ]
    settings = {
        'DDDBTag': 'MC11-20111102',
        'CondDBTag': 'sim-20111111-vc-mu100',
        'XMLFileCatalog': 'pool_xml_catalog.xml',
        'OutputFileTypes': ['digi'],
        'InputFiles': ["PFN:" + TestStep2.outputFile()],
    }


class TestStep4(StepTest):
    application = ("Brunel", "v41r1p1")
    step = 4
    packages = 'AppConfig.v3r122;SQLDDDB.v6r20'
    options = [
        '$APPCONFIGOPTS/Brunel/DataType-2011.py',
        '$APPCONFIGOPTS/Brunel/MC-WithTruth.py',
        '$APPCONFIGOPTS/Brunel/MC11a_dedxCorrection.py',
        '$APPCONFIGOPTS/Brunel/TrigMuonRawEventFix.py'
    ]
    settings = {
        'DDDBTag': 'MC11-20111102',
        'CondDBTag': 'sim-20111111-vc-mu100',
        'XMLFileCatalog': 'pool_xml_catalog.xml',
        'OutputFileTypes': ['dst'],
        'InputFiles': ["PFN:" + TestStep3.outputFile()],
    }


class TestStep5(StepTest):
    application = ("DaVinci", "v29r1p1")
    step = 5
    packages = 'AppConfig.v3r116;SQLDDDB.v6r20'
    options = [
        '$APPCONFIGOPTS/DaVinci/DV-Stripping17-Stripping-MC-NoPrescaling.py'
    ]
    settings = {
        'DDDBTag': 'MC11-20111102',
        'CondDBTag': 'sim-20111111-vc-mu100',
        'XMLFileCatalog': 'pool_xml_catalog.xml',
        'OutputFileTypes': ['AllStreams.dst'],
        'InputFiles': ["PFN:" + TestStep4.outputFile()],
        'NOfEvents': 3,
    }


class TestStep6(StepTest):
    application = ("LHCb", "v33r1")
    step = 6
    packages = 'AppConfig.v3r118'
    options = ['$APPCONFIGOPTS/Merging/CopyDST.py']
    settings = {
        'DDDBTag': 'none',
        'CondDBTag': 'none',
        'XMLFileCatalog': 'pool_xml_catalog.xml',
        'OutputFileTypes': ['allstreams.dst'],
        'InputFiles': ["PFN:" + TestStep5.outputFile()],
    }

    # Ignore HistogramFile
    def test_HistogramFile(self):
        pass

    test_HistogramFile.__test__ = False
