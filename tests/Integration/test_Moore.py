###############################################################################
# (c) Copyright 2012-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from TestTools import ConfigTest


class TestConfig_v12r8g1(ConfigTest):
    _cmtconfigs = ['i686-slc5-gcc43-opt', 'x86_64-slc5-gcc43-opt']
    application = ("Moore", "v12r8g1")
    packages = 'AppConfig.v3r120;SQLDDDB.v6r20'
    options = [
        '$APPCONFIGOPTS/Moore/MooreSimProduction.py',
        '$APPCONFIGOPTS/Conditions/TCK-0x40760037.py',
        '$APPCONFIGOPTS/Moore/DataType-2011.py'
    ]
    settings = {
        'DDDBTag': 'MC11-20111102',
        'CondDBTag': 'sim-20111111-vc-mu100',
        'OutputFilePrefix': '00000000_00000000_0',
        'OutputFileTypes': ['digi'],
        'XMLSummaryFile': 'summaryMoore_00000000_00000000_0.xml',
        'XMLFileCatalog': 'pool_xml_catalog.xml',
        'InputFiles': ["PFN:some_input.digi"],
        'NOfEvents': 234,
    }
