###############################################################################
# (c) Copyright 2012-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Module containing helper functions and classes to run integration tests on
ProdConf.
"""
__author__ = "Marco Clemencic <marco.clemencic@cern.ch>"

import os
import shutil
import subprocess
import nose

from LbConfiguration.Platform import NativeMachine
# List of platforms usable on the host
platforms = NativeMachine().CMTSupportedConfig()


def splitPackageList(s):
    '''
    Transform a package list string in the format "Pkg1.vXrY;Pkg2.vZrW;..." in
    a list of pairs.

    >>> splitPackageList("Pkg1.vXrY;Pkg2.vZrW")
    [('Pkg1', 'vXrY'), ('Pkg2', 'vZrW')]
    '''
    return [tuple(pv.strip().split('.')) for pv in s.split(';')]


class ConfigTest(object):
    """
    Helper base class to prepare fixtures for integration tests of ProdConf.

    The test, by default, prepare a job and executes it in dry-run mode to dump
    the produced options. The tests will have access to the configuration as a
    dictionary in self.opts_dump.

    To create a new test this class must be subclassed adding the following
    class attributes:

    @param application: pairs declaring the name and version of the application
                        to be used, e.g. ("Gauss", "v41r1")
    @param packages: list of pairs declaring the software packages needed for the
                     test without ProdConf, e.g. [("AppConfig", "v3r118"), ("DecFiles", "v25r0")].
                     The list can also be specified as a string like "AppConfig.v3r118;DecFiles.v25r0".
    @param options: list of option files to be passed to gaudirun.py on the
                    command line, e.g. ["$APPCONFIGOPTS/Gauss/Beam3500GeV-md100-MC11-nu2.py",
                    "$DECFILESROOT/options/13144003.py", "$LBPYTHIAROOT/options/Pythia.py",
                    "$APPCONFIGOPTS/Gauss/G4PL_LHEP_EmNoCuts.py"]
    @param settings: dictionary of arguments to be passed to the ProdConf constructor
    @param _workdir: optional working directory to use, if not specified, a temporary
                     one is created.
    """

    # Defaults
    application = (None, None)
    packages = []
    options = []
    settings = {}

    # Internal parameters
    _cmtconfigs = None  # any platform
    _workdir = None
    _gaudirun = 'gaudirun.py'
    _gaudirunOpts = ['-v', '-n']

    # Test results
    returnCode = None
    opts_dump = {}

    @classmethod
    def privateFilename(cls, name):
        """
        Modify the provided filename adding to it the name of the class (needed
        to allow several tests in the same working directory.
        """
        return "%%s_%s%%s" % cls.__name__ % os.path.splitext(name)

    @classmethod
    def setupAll(cls):
        """
        Prepare the test working directory with the shell script to run.
        """
        from os import pardir
        from os.path import join, dirname

        # add standard settings if missing
        app, vers = cls.application
        for k, v in [('Application', app), ('AppVersion', vers)]:
            cls.settings.setdefault(k, v)

        # check if we have a defined working directory
        if cls.getWorkdir() is None:
            # create a temporary one and store it in the private variable
            from tempfile import mkdtemp
            cls._workdir = mkdtemp('_ProdConfTest')
            cls._cleanWorkdir = True
        else:
            # just ensure that the needed work directory exists
            if not os.path.isdir(cls.getWorkdir()):
                os.makedirs(cls.getWorkdir())
            cls._cleanWorkdir = False

        # transform the list of packages into a list, if needed
        if type(cls.packages) is str:
            cls.packages = splitPackageList(cls.packages)

        # write the option file for ProdConf
        prodConf_opts = cls.privateFilename('prodConf_opts.py')
        options = open(cls.localFile(prodConf_opts), 'w')
        options.write("from ProdConf import ProdConf\n\nProdConf(")
        options.writelines(["  %s=%r,\n" % i for i in cls.settings.items()])
        options.write(")\n")
        options.close()

        # prepare commands for the script
        scriptLines = []
        if cls._cmtconfigs is not None:  # if we have limitations on the platforms
            # get the first of the supported ones
            c = [p for p in platforms if p in cls._cmtconfigs]
            if c:
                scriptLines.append('export CMTCONFIG={0}'.format(c[0]))
            else:
                raise nose.SkipTest('%s.%s: platform not supported' %
                                    (cls.__module__, cls.__name__))
        # - lb-run
        spCmd = 'eval $(lb-run --sh --no-user {pkgs} {app[0]}/{app[1]})'
        scriptLines.append(
            spCmd.format(
                app=cls.application,
                pkgs=' '.join(
                    ['--use "%s %s"' % pkg for pkg in cls.packages])))
        # - add the current location to the PYTHONPATH
        #   it is `basename $0`/../../python
        mod_dir = join(dirname(__file__), pardir, pardir, "python")
        envExt = 'export PYTHONPATH=%s:$PYTHONPATH' % mod_dir
        scriptLines.append(envExt)
        scriptLines.append(
            'export PYTHONHOME=$(dirname $(dirname $(which python)))')
        scriptLines.append('printenv | sort')
        # - call gaudirun.py
        grCmd = ' '.join(
            [cls._gaudirun, '-o',
             cls.privateFilename('opts_dump.py')] + cls._gaudirunOpts +
            cls.options + [prodConf_opts])
        scriptLines.append(grCmd)

        # write the script
        script = open(cls.localFile(cls.privateFilename('run_test.sh')), 'w')
        script.writelines([l + "\n" for l in scriptLines])
        script.close()

        # run the application (it's part of the class setup)
        cls.returnCode = cls.run()

        # collect the dumped options
        try:
            cls.opts_dump = eval(
                open(cls.localFile(
                    cls.privateFilename('opts_dump.py'))).read())
        except:
            cls.opts_dump = {}

    @classmethod
    def teardownAll(cls):
        """
        Remove the temporary directory after the execution of the test if it has
        been generated automatically.
        """
        if cls._cleanWorkdir:
            shutil.rmtree(cls._workdir, ignore_errors=True)

    @classmethod
    def getWorkdir(cls):
        """
        Return the path to the work directory.

        Can be overloaded in derived classes to get the working directory in a
        different way (e.g. from the module).
        """
        return cls._workdir

    @classmethod
    def run(cls):
        """
        Run the generated script and return its return code.
        """
        stdout = open(cls.localFile(cls.privateFilename("stdout.log")), "w")
        stderr = open(cls.localFile(cls.privateFilename("stderr.log")), "w")
        return subprocess.call(
            ["bash", cls.privateFilename('run_test.sh')],
            cwd=cls.getWorkdir(),
            stdout=stdout,
            stderr=stderr)

    @classmethod
    def localFile(cls, name, *args):
        """
        Return the full path to a local file in the working directory.

        When several arguments are passed, it behaves as os.path.join.
        """
        return os.path.join(cls.getWorkdir(), name, *args)

    @classmethod
    def privateFile(cls, name, *args):
        """
        Return the full path to a local file in the working directory.

        When several arguments are passed, it behaves as os.path.join.
        """
        return cls.localFile(name, *args)

    @classmethod
    def outputFile(cls):
        """
        Return the expected output file name of the job.
        """
        return cls.settings["OutputFilePrefix"] + '.' + cls.settings[
            "OutputFileTypes"][0]

    def checkOpt(self, setting, opt_path, comp=None):
        '''
        Helper function to check an option value.

        @param setting: name of the option in the settings class attribute
        @param opt_path: list of keys pointing to the option in the options dump
        @param comp: callable comparing two arguments (found, expected), it must
                    return True if the found value is equivalent to the expected one;
                    the default (None) is equivalent to '=='
        '''
        if comp is None:
            comp = lambda f, e: f == e
        if setting in self.settings:
            expected = self.settings[setting]
            try:
                found = self.opts_dump
                for k in opt_path:
                    found = found[k]
            except KeyError, k:
                raise AssertionError("Option %s not found (missing %s)" %
                                     ('.'.join(opt_path), k))
            assert comp(found,
                        expected), 'option %s != setting %s (%r != %r)' % (
                            '.'.join(opt_path), setting, found, expected)

    def test_ReturnCode(self):
        assert self.returnCode == 0, "Bad application return code %d" % self.returnCode

    def test_CommonOptions(self):
        condDB = 'LHCBCOND'
        if ('CondDBCnvSvc' in self.opts_dump and 'Simulation' in self.
                opts_dump['CondDBCnvSvc']['CondDBReader']):
            condDB = 'SIMCOND'

        base_desc = '.'.join([
            self.__class__.__module__, self.__class__.__name__,
            'test_CommonOptions'
        ])
        for args in [
            ('NOfEvents', ['ApplicationMgr', 'EvtMax']),
            ('HistogramFile', ['HistogramPersistencySvc', 'OutputFile']),
            ('DDDBTag', ['DDDB', 'DefaultTAG']),
            ('CondDBTag', [condDB, 'DefaultTAG']),
            ('DQTag', ['DQFLAGS', 'DefaultTAG']),
            ('XMLSummaryFile', ['CounterSummarySvc', 'xmlfile']),
            ('XMLFileCatalog', ['FileCatalog', 'Catalogs'],
             lambda f, e: ('xmlcatalog_file:' + e) in f),
        ]:
            # Helper function to have a nice test name (in nosetests -v)
            # note: it needs to be inside the loop to play nice with nosetests -v --collect-only
            check = lambda *args: self.checkOpt(*args)
            check.description = base_desc + '.' + args[0]
            yield (check, ) + args

        def messageSvcOpts():
            assert self.opts_dump['MessageSvc']['Format'] == '%u % F%18W%S%7W%R%T %0W%M', \
              "Wrong MessageSvc.Format: %s, expected %s" % (self.opts_dump['MessageSvc']['Format'], '%u % F%18W%S%7W%R%T %0W%M')
            assert self.opts_dump['MessageSvc']['timeFormat'] == '%Y-%m-%d %H:%M:%S UTC', \
              "Wrong MessageSvc.timeFormat: %s, expected %s" % (self.opts_dump['MessageSvc']['timeFormat'], '%Y-%m-%d %H:%M:%S UTC')

        messageSvcOpts.description = base_desc + '.' + "messageSvcOpts"
        yield messageSvcOpts

    def checkOutputOptions(self, outputStreamFilter):
        # Test that the output file is correctly configured in the detected output streams.
        ostreams = filter(outputStreamFilter, self.opts_dump)
        assert ostreams, "No output streams found"
        for n in ostreams:
            c = self.opts_dump[n]
            if "Output" in c:
                assert self.outputFile() in c[
                    "Output"], "Wrong Output in %s: %r (must contain %s)" % (
                        n, c["Output"], self.outputFile())


class IntegrationTest(ConfigTest):
    """
    Helper base class to prepare fixtures for integration tests of ProdConf.
    Same as ConfigTest, but the application is actually run, so also the products
    can be tested.

    To create a new test this class must be subclassed adding the following
    class attributes:

    @param application: pairs declaring the name and version of the application
                        to be used, e.g. ("Gauss", "v41r1")
    @param packages: list of pairs declaring the software packages needed for the
                     test without ProdConf, e.g. [("AppConfig", "v3r118"), ("DecFiles", "v25r0")].
                     The list can also be specified as a string like "AppConfig.v3r118;DecFiles.v25r0".
    @param options: list of option files to be passed to gaudirun.py on the
                    command line, e.g. ["$APPCONFIGOPTS/Gauss/Beam3500GeV-md100-MC11-nu2.py",
                    "$DECFILESROOT/options/13144003.py", "$LBPYTHIAROOT/options/Pythia.py",
                    "$APPCONFIGOPTS/Gauss/G4PL_LHEP_EmNoCuts.py"]
    @param settings: dictionary of arguments to be passed to the ProdConf constructor
    @param workdir: optional working directory to use, if not specified, a temporary
                    one is created.
    """

    # Internal parameters
    _gaudirunOpts = ['-v']

    def test_XMLSummary(self):
        if "XMLSummaryFile" in self.settings:
            from xml.dom.minidom import parse
            parse(self.localFile(self.settings["XMLSummaryFile"]))

    def test_HistogramFile(self):
        if "HistogramFile" in self.settings:
            assert os.path.exists(
                self.localFile(self.settings["HistogramFile"])
            ), "Expected histograms file not found: %s" % self.settings[
                "HistogramFile"]

    def test_XMLFileCatalog(self):
        if "XMLFileCatalog" in self.settings:
            from xml.dom.minidom import parse
            data = parse(self.localFile(self.settings["XMLFileCatalog"]))
            pfns = [
                e.getAttribute('name')
                for e in data.getElementsByTagName('pfn')
            ]
            assert self.outputFile(
            ) in pfns, "Expected output file not found in file catalog: %s" % self.outputFile(
            )

    def test_OutputFile(self):
        assert os.path.exists(self.localFile(self.outputFile(
        ))), "Expected output file not found: %s" % self.outputFile()


class StepTest(IntegrationTest):
    """
    Specialization of IntegrationTest to simplify the test of a production step.

    The key feature is the use of the common working directory for the module.
    """

    step = 1
    prodId = 1
    jobId = 1

    @classmethod
    def prefix(cls):
        return '%08d_%08d_%d' % (cls.prodId, cls.jobId, cls.step)

    @classmethod
    def outputFile(cls):
        """
        Return the expected output file name of the job.
        """
        return cls.prefix() + '.' + cls.settings["OutputFileTypes"][0]

    @classmethod
    def setupAll(cls):
        """
        Set some default ProdConf settings before calling the default setup.
        """
        # prepare the default values
        prefix = cls.prefix()

        # add standard settings if missing
        app, vers = cls.application
        for k, v in [('Application', app), ('AppVersion', vers),
                     ('HistogramFile', '%s_%s_Hist.root' % (app, prefix)),
                     ('XMLSummaryFile', 'summary%s_%s.xml' % (app, prefix)),
                     ('OutputFilePrefix', prefix)]:
            cls.settings.setdefault(k, v)

        super(StepTest, cls).setupAll()
