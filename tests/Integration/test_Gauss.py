###############################################################################
# (c) Copyright 2012-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from TestTools import ConfigTest


class GaussTestClass(ConfigTest):
    # application = ("Gauss", "v41r1")
    packages = [("AppConfig", "v3r118"), ("DecFiles", "v25r0")]
    options = [
        "$APPCONFIGOPTS/Gauss/Beam3500GeV-md100-MC11-nu2.py",
        "$DECFILESROOT/options/13144003.py", "$LBPYTHIAROOT/options/Pythia.py",
        "$APPCONFIGOPTS/Gauss/G4PL_LHEP_EmNoCuts.py"
    ]
    settings = {
        'HistogramFile': 'Gauss_00012345_00067890_1_Hist.root',
        'XMLSummaryFile': 'summaryGauss_00012345_00067890_1.xml',
        'XMLFileCatalog': 'pool_xml_catalog.xml',
        'OutputFilePrefix': '00012345_00067890_1',
        'OutputFileTypes': ['sim'],
        'DDDBTag': 'MC11-20111102',
        'CondDBTag': 'sim-20111111-vc-md100',
        'RunNumber': 1302390,
        'FirstEventNumber': 203668,
        'NOfEvents': 3,
    }

    def test_GaussOpts(self):
        pass


class TestConfig_v41r1(GaussTestClass):
    _cmtconfigs = ['i686-slc5-gcc43-opt', 'x86_64-slc5-gcc43-opt']
    application = ("Gauss", "v41r1")


class TestConfig_v41r2(GaussTestClass):
    _cmtconfigs = ['i686-slc5-gcc43-opt', 'x86_64-slc5-gcc43-opt']
    application = ("Gauss", "v41r2")
