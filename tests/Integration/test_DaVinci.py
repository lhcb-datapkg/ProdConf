###############################################################################
# (c) Copyright 2012-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from TestTools import ConfigTest


class TestConfig_v29r1p1_Str17(ConfigTest):
    _cmtconfigs = ['i686-slc5-gcc43-opt', 'x86_64-slc5-gcc43-opt']
    application = ("DaVinci", "v29r1p1")
    options = [
        '$APPCONFIGOPTS/DaVinci/DV-Stripping17-Stripping-MC-NoPrescaling.py'
    ]
    settings = {
        'HistogramFile': 'DaVinci_00000000_00000000_0_Hist.root',
        'DDDBTag': 'MC11-20111102',
        'CondDBTag': 'sim-20111111-vc-md100',
        'XMLSummaryFile': 'summaryDaVinci_00000000_00000000_0.xml',
        'OutputFilePrefix': '00000000_00000000_0',
        'XMLFileCatalog': 'pool_xml_catalog.xml',
        'OutputFileTypes': ['allstreams.dst'],
        'InputFiles': ['PFN:dummy_input.dst'],
        'NOfEvents': 100,
    }


class MCStrippingConfigTest(ConfigTest):
    def test_OutputOptions(self):
        self.checkOutputOptions(lambda n: n.endswith("_OStream"))


class TestConfig_v29r3p1_Str17_p7590(MCStrippingConfigTest):
    _cmtconfigs = ['i686-slc5-gcc43-opt', 'x86_64-slc5-gcc43-opt']
    application = ("DaVinci", "v29r3p1")
    packages = 'RDConfig.v1r2'
    options = ['$RDCONFIGOPTS/FilterKstmmIPS.py']
    settings = {
        'HistogramFile': 'DaVinci_00000000_00000000_0_Hist.root',
        'DDDBTag': 'MC11-20111102',
        'CondDBTag': 'sim-20111111-vc-mu100',
        'XMLSummaryFile': 'summaryDaVinci_00000000_00000000_0.xml',
        'OutputFilePrefix': '00000000_00000000_0',
        'XMLFileCatalog': 'pool_xml_catalog.xml',
        'OutputFileTypes': ['Kstmm.StripTrig.dst'],
        'InputFiles': ["PFN:dummy_input.dst"],
        'NOfEvents': 3,
    }


class TestConfig_v29r2p2_Str17_p7574(MCStrippingConfigTest):
    _cmtconfigs = ['i686-slc5-gcc43-opt', 'x86_64-slc5-gcc43-opt']
    application = ("DaVinci", "v29r2p2")
    packages = 'CharmConfig.v2r3'
    options = [
        '$CHARMCONFIGROOT/scripts/CharmWGMCFiltering/FinalMC11aScripts/RunS17/PrescalesOn/CharmGroup-DV-Stripping17-Stripping-MC-PrescalesS17.py'
    ]
    settings = {
        'HistogramFile': 'DaVinci_00000000_00000000_0_Hist.root',
        'DDDBTag': 'MC11-20111102',
        'CondDBTag': 'sim-20111111-vc-mu100',
        'XMLSummaryFile': 'summaryDaVinci_00000000_00000000_0.xml',
        'OutputFilePrefix': '00000000_00000000_0',
        'XMLFileCatalog': 'pool_xml_catalog.xml',
        'OutputFileTypes': ['Dst_D0pi.Strip.Trig.dst'],
        'InputFiles': ["PFN:dummy_input.dst"],
        'NOfEvents': 3,
    }


class TestConfig_v30r2p2_FmDST_p17548(ConfigTest):
    _cmtconfigs = [
        'i686-slc5-gcc43-opt', 'x86_64-slc5-gcc43-opt', 'x86_64-slc5-gcc46-opt'
    ]
    application = ("DaVinci", "v30r2p2")
    packages = 'AppConfig.v3r130'
    options = ['$APPCONFIGOPTS/Kali/KaliPi0_producefmDST.py']
    settings = {
        'OptionFormat': 'femto',
        'DDDBTag': 'head-20120413',
        'CondDBTag': 'head-20120413',
        'XMLSummaryFile': 'summaryDaVinci_00017548_00000021_1.xml',
        'OutputFilePrefix': '00017548_00000021_1',
        'XMLFileCatalog': 'pool_xml_catalog.xml',
        'OutputFileTypes': ['fmdst'],
        'InputFiles': ["LFN:dummy_input.dst"],
        'RunNumber': 113049,
        'NOfEvents': 3,
    }

    def test_OutputOptions(self):
        self.checkOutputOptions(lambda n: n.endswith("FMDST"))

    def test_LumiSeq(self):
        # Check that the LumiSeq sequencer is empty
        # see task #30221 https://savannah.cern.ch/task/?30221
        LumiSeq = self.opts_dump["LumiSeq"]
        assert not LumiSeq.get("Members"), "LumiSeq is not empty"


class TestConfig_v29r3p1_task30221(ConfigTest):
    """https://savannah.cern.ch/task/?30221"""
    _cmtconfigs = ['i686-slc5-gcc43-opt', 'x86_64-slc5-gcc43-opt']
    application = ("DaVinci", "v29r3p1")
    packages = 'WG/RDConfig.v1r4'
    options = ['$RDCONFIGOPTS/FilterKS02MuMu.py']
    settings = {
        'OptionFormat': 'None',
        'DDDBTag': 'MC11-20111102',
        'CondDBTag': 'sim-20111111-vc-md100',
        'XMLSummaryFile': 'summaryDaVinci_00012345_00067890_5.xml',
        'OutputFilePrefix': '00012345_00067890_5',
        'XMLFileCatalog': 'pool_xml_catalog.xml',
        'OutputFileTypes': ['kstmm.striptrig.dst'],
        'InputFiles': ["LFN:dummy_input.dst"],
        'NOfEvents': -1,
    }

    def test_LumiSeq(self):
        # Check that the LumiSeq sequencer is not empty
        # see task #30221 https://savannah.cern.ch/task/?30221
        LumiSeq = self.opts_dump["LumiSeq"]
        assert LumiSeq.get("Members"), "LumiSeq is empty"
