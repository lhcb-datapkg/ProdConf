###############################################################################
# (c) Copyright 2012-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from TestTools import ConfigTest


class TestConfig_Moore_v12r5p2(ConfigTest):
    _cmtconfigs = ['i686-slc5-gcc43-opt', 'x86_64-slc5-gcc43-opt']
    application = ("Moore", "v12r5p2")
    packages = 'WG/CharmConfig.v2r2'
    options = [
        '$APPCONFIGOPTS/EnableCustomMainLoop.py',
        '$CHARMCONFIGROOT/scripts/SwimTriggerD2kp.py'
    ]
    settings = {
        'OptionFormat': 'swimming2011',
        'DDDBTag':
        'head-20110302',  # retrieved from $CHARMCONFIGROOT/db/tag_database.db for run 89333
        'CondDBTag':
        'head-20110331',  # retrieved from $CHARMCONFIGROOT/db/tag_database.db for run 89333
        'OutputFilePrefix': '00000000_00000000_0',
        'OutputFileTypes': ['swimtriggerd02kpi.dst'],
        'XMLSummaryFile': 'summaryMoore_00000000_00000000_0.xml',
        'XMLFileCatalog': 'pool_xml_catalog.xml',
        'InputFiles': ["PFN:some_input.dst"],
        'RunNumber': 89333,
        'NOfEvents': 200,
    }

    # Un-comment to fix the temporary working directory
    #_workdir = "/tmp/test_Swimming_conf"

    def test_OutputOptions(self):
        self.checkOutputOptions(lambda n: n.endswith("Writer"))


class TestConfig_DaVinci_v29r2p4(ConfigTest):
    _cmtconfigs = ['i686-slc5-gcc43-opt', 'x86_64-slc5-gcc43-opt']
    application = ("DaVinci", "v29r2p4")
    packages = 'WG/CharmConfig.v2r2'
    options = [
        '$APPCONFIGOPTS/EnableCustomMainLoop.py',
        '$CHARMCONFIGROOT/scripts/SwimStrippingD2kp.py'
    ]
    settings = {
        'OptionFormat': 'swimming2011',
        'DDDBTag': 'test-dddb',
        'CondDBTag': 'test-conddb',
        'OutputFilePrefix': '00000000_00000000_0',
        'OutputFileTypes': ['swimstrippingd02kpi.mdst'],
        'XMLSummaryFile': 'summaryDaVinci_00000000_00000000_0.xml',
        'XMLFileCatalog': 'pool_xml_catalog.xml',
        'InputFiles': ["PFN:some_input.dst"],
        #'RunNumber': 89333,
        'NOfEvents': 20,
    }

    # Un-comment to fix the temporary working directory
    #_workdir = "/tmp/test_Swimming_conf"

    def test_OutputOptions(self):
        self.checkOutputOptions(lambda n: n.endswith("_OStream"))
