###############################################################################
# (c) Copyright 2012-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from TestTools import ConfigTest


class TestConfig_v33r1_merge(ConfigTest):
    _cmtconfigs = ['i686-slc5-gcc43-opt', 'x86_64-slc5-gcc43-opt']
    application = ("LHCb", "v33r1")
    packages = 'AppConfig.v3r118'
    options = ['$APPCONFIGOPTS/Merging/CopyDST.py']
    settings = {
        'DDDBTag': 'HEAD',
        'CondDBTag': 'HEAD',
        'OutputFilePrefix': '00000000_00000000_0',
        'XMLFileCatalog': 'pool_xml_catalog.xml',
        'OutputFileTypes': ['allstreams.dst'],
        'InputFiles': ["PFN:some_prefix.AllStreams.dst"],
    }
