###############################################################################
# (c) Copyright 2012-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from TestTools import ConfigTest


class TestConfig_v41r1p1(ConfigTest):
    _cmtconfigs = ['i686-slc5-gcc43-opt', 'x86_64-slc5-gcc43-opt']
    application = ("Brunel", "v41r1p1")
    packages = 'AppConfig.v3r122;SQLDDDB.v6r20'
    options = [
        '$APPCONFIGOPTS/Brunel/DataType-2011.py',
        '$APPCONFIGOPTS/Brunel/MC-WithTruth.py',
        '$APPCONFIGOPTS/Brunel/MC11a_dedxCorrection.py',
        '$APPCONFIGOPTS/Brunel/TrigMuonRawEventFix.py'
    ]
    settings = {
        'DDDBTag': 'MC11-20111102',
        'CondDBTag': 'sim-20111111-vc-mu100',
        'OutputFilePrefix': '00000000_00000000_0',
        'OutputFileTypes': ['dst'],
        'XMLSummaryFile': 'summaryBrunel_00000000_00000000_0.xml',
        'XMLFileCatalog': 'pool_xml_catalog.xml',
        'InputFiles': ["PFN:some_input.digi"],
        'NOfEvents': 234,
    }


class TestConfig_v44r9(ConfigTest):
    _cmtconfigs = [
        'x86_64-slc5-gcc43-opt', 'x86_64-slc5-gcc46-opt',
        'x86_64-slc6-gcc46-opt'
    ]
    application = ("Brunel", "v44r9")
    packages = 'AppConfig.v3r162;SQLDDDB.v7r9'
    options = [
        '$APPCONFIGOPTS/Brunel/DataType-2011.py',
        '$APPCONFIGOPTS/Brunel/MC-WithTruth.py',
        '$APPCONFIGOPTS/Brunel/MC11a_dedxCorrection.py',
        '$APPCONFIGOPTS/Brunel/TrigMuonRawEventFix.py'
    ]
    settings = {
        'DDDBTag': 'MC11-20111102',
        'CondDBTag': 'sim-20111111-vc-mu100',
        'OutputFilePrefix': '00000000_00000000_0',
        'OutputFileTypes': ['dst'],
        'XMLSummaryFile': 'summaryBrunel_00000000_00000000_0.xml',
        'XMLFileCatalog': 'pool_xml_catalog.xml',
        'InputFiles': ["PFN:some_input.digi"],
        'NOfEvents': 234,
    }
