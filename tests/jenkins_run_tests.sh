#!/bin/bash
###############################################################################
# (c) Copyright 2012-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

# Usage:
#   jenkins_run_tests.sh [test_suite]
#
# where test_suite can be Unit or Integration (or any of the test suites
# in the tests directory)

shopt -s expand_aliases

if [ -e /cvmfs/lhcb.cern.ch/lib/lhcb/LBSCRIPTS/prod/InstallArea/scripts/LbLogin.sh ] ; then
    . /cvmfs/lhcb.cern.ch/lib/lhcb/LBSCRIPTS/prod/InstallArea/scripts/LbLogin.sh
else
  if [ -e /afs/cern.ch/lhcb/software/releases/LBSCRIPTS/prod/InstallArea/scripts/LbLogin.sh ] ; then
    . /afs/cern.ch/lhcb/software/releases/LBSCRIPTS/prod/InstallArea/scripts/LbLogin.sh
  else
    echo "ERROR: Cannot set up the LHCb environment (no /cvmfs or /afs ?)"
    exit 1
  fi
fi

# workaround for an issue with bash and exported functions
unset module
eval $(lb-run --no-user --sh Gaudi/latest)

if [ "$WORKSPACE" = "" ] ; then
  export WORKSPACE=$(cd $(dirname $0) ; cd .. ; pwd)
fi

set -ex
export PYTHONPATH=$WORKSPACE/python:$PYTHONPATH

nosetests -w tests -v --with-coverage --cover-package=ProdConf --with-xunit $*

# note: coverage and pylint are not run if the tests fail

coverage xml --include="python/*"

# Set the environment for pylint run
export PYTHONPATH=$WORKSPACE/tests/Unit/python:$PYTHONPATH
# Disabled:
# - C: coding convention (we need to prepare a pylint rc file)
# - R: refactoring (to be fixed with the rc file)
# - W0511: FIXMEs (I'm using FIXMEs as reminders)
# (ignoring return code)
pylint --output-format parseable --disable C,R,W0511 ProdConf > pylint.txt || true
